# DV SLAM

This repository contains a wrapped [ESVO](https://github.com/HKUST-Aerial-Robotics/ESVO.git) SLAM algorithm for DV
software. The implementation can be regarded as a port from ROS framework to DV with performance improvements.

The highlights of this implementation are the following:
* DVXplorer resolution (640x480) support. 
> Testing has shown that original implementation in ROS was only able to produce
20~30 FPS of accumulated time surfaces, while highly optimized DV modules achieves 100 FPS on the same hardware.
* Optional GPU support.
> Some calculations in mapping thread can be offloaded to the GPU, that improves the ability to run the algorithm 
> on embedded devices with NVIDIA GPU (such as the Jetson product line).
* Optimized tracking thread.
> Code optimizations provides up to 10x faster execution of tracking thread. See [performance analysis](docs/performance.md) 
> for more details.

## Camera Requirements

Algorithm requires requires two rigidly mounted cameras with a synchronization cable attached and accurate stereo calibration. The cameras that support multi camera synchronization: DVXplorer, DVXplorer Lite, and DAVIS346. 


| Important: DVXplorer Mini does not support time synchronization and it is not supported for this algorithm. |
| ------ | 



## Preview

The algorithm was tested on real data from stereo camera (two DVXplorer sensors mounted on a plate):

![Stereo Camera](docs/images/camera.jpg)

The reconstructed trajectory: 

![](docs/videos/on_robot.mp4)

Previews of module running on datasets provided by the original authors (upenn_flying1, upenn_flying3, rpg_desk):

![](docs/videos/datasets.mp4) 

## Getting started

Tutorials for the SLAM module can be found in `docs` directory.

Build tutorial for the repository is available [here](docs/build.md).

Achieving any results with this algorithm requires accurate camera calibration, tutorial on how to perform 
calibration is available [here](docs/calibration.md).

To get up and running, please take follow [first run](docs/running-slam.md) and additional [parameter tuning](docs/tuning.md) 
tutorials. 

Sample datasets to test the algorithm can be downloaded from [here](https://release.inivation.com/datasets/dv-stereo-slam/?prefix=datasets/dv-stereo-slam/). 
A step by step guide on how to run and evaluate the datasets is available in the [datasets tutorial](docs/datasets.md).

Details on performance metrics of the implementation can be found [here](docs/performance.md).


## Paper References

* **[Event-based Stereo Visual Odometry](https://arxiv.org/abs/2007.15548)**, *Yi Zhou, Guillermo Gallego, Shaojie Shen*, arXiv preprint 2020 (under review).

* **[Semi-dense 3D Reconstruction with a Stereo Event Camera](https://arxiv.org/abs/1807.07429)**, *Yi Zhou, Guillermo Gallego, Henri Rebecq, Laurent Kneip, Hongdong Li, Davide Scaramuzza*, ECCV 2018.

