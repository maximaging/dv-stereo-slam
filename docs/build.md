# Build instructions

## Dependencies

Clone the repository, initialize submodules from project repository:
```
git submodule update --init --recursive
```

### Ubuntu (tested on 18.04 and 20.04)

Add the inivation PPA, for Ubuntu 18.04:
```
sudo add-apt-repository ppa:inivation-ppa/inivation-bionic
```
For Ubuntu 20.04:
```
sudo add-apt-repository ppa:inivation-ppa/inivation
```

Install dependencies;
```
sudo apt-get update
sudo apt install libopencv-dev libcaer-dev dv-gui dv-runtime-dev libeigen3-dev libyaml-cpp-dev libpcl-dev libgoogle-glog-dev
```

Octomap library can be installed to generate an output octomap of the environment:
```
sudo apt install liboctomap-dev
```

### OSX (tested on 11.4)

```
brew tap inivation/inivation
brew install libcaer dv-runtime opencv eigen yaml-cpp pcl glog
```

Octomap library can be installed to generate an output octomap of the environment:
```
brew install octomap
```

## GPU Version

Make sure you have NVIDIA drivers and CUDA libraries installed, the current implementation was tested with CUDA 10.2 with GeForce RTX 2060 Super and Jetson Xavier NX.
To build with CUDA acceleration enabled, use following commands in the project root directory:

```
mkdir build
cd build
cmake -DUSE_CUDA_STEREO=ON ..
make
sudo make install
```

## CPU Only Version

Build as any usual cmake project:

```
mkdir build
cd build
cmake ..
make
sudo make install
```
