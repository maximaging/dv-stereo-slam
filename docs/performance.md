# Results of performance testing

## Accuracy 

This implementation does not depend on ROS, some parts were replaced by substitute libraries (e.g. ROS tf2 library is 
replaced with simplified usage of `minkindr`). The output of the algorithm is slightly different from the original 
implementation, accuracy tests were performed to evaluate the possible differences.

Table below provides absolute translational error (RMS) values in centimeters.

| Dataset       | ORB_SLAM2 (paper) | ESVO (paper) | ROS  | DV, with GPU | DV, with GPU and motion compensation |
|---------------|-------------------|--------------|------|--------------|--------------------------------------|
| rpg_desk      | 7.7               | 3.2          | 3.1  | 3.3          | 3.7                                  |
| upenn_flying1 | 49.8              | 13.9         | 19.2 | 9.5          | 20.7                                 |
| upenn_flying3 | 50.2              | 11.1         | 12.9 | 7.3          | 13.4                                 |

> The columns marked with `(paper)` shows accuracy values provided in the paper of the authors.

> Column `ROS` contains accuracy values from original open source implementation, measured using 
> [trajectory evaluation tool](https://github.com/uzh-rpg/rpg_trajectory_evaluation.git) which was also used to evaluate 
> output from this implementation.

> Last column uses motion compensation instead of exponential accumulator to generate input frames.

## Execution Speed

### Desktop PC Performance

Performance is important for SLAM algorithms, since applications performing SLAM might have size constrained, it is
important to be able to run it on embedded platform. Average execution speed in milliseconds was measured of each part 
of the algorithm to measure the difference between original ROS implementation and DV.

Table below provides time of execution of each part of algorithm (average time in milliseconds per iteration).

Camera sensor resolution used in this test is 640x480.

| Module                    | ROS, milliseconds (FPS) | DV, milliseconds (FPS) | Speedup |
|---------------------------|-------------------------|------------------------|---------|
| Accumulator (exponential) | 44.2 (23)               | 5.90 (169)             | 7.49    |
| Tracking                  | 8.58 (116)              | 0.92 (1092)            | 9.37    |
| Mapping                   | 51.6 (19)               | 23.5 (43)              | 2.20    |

Speed measurements were performed on while running same data and same configuration (as close as possible) on a 
computer with Ryzen 5 3600 CPU and NVIDIA RTX 2060 Super GPU.

### Jetson Xavier NX Performance
  
To verify performance on an embedded system, Jetson Xavier NX was used to do performance testing.

Camera sensor resolution used in this test is 346x240.

| Module                    | DV, milliseconds       | FPS |
|---------------------------|------------------------|-----|
| Tracking                  | 2.45                   | 409 |
| Mapping                   | 55.5                   | 18  |

The implementation is able to run on the Jetson Xavier NX platform with 100 FPS tracking and 
20 FPS mapping. Due to task parallel implementation, the mapping thread achieves ~25 FPS, the 
measurements above gives single frame computation time, when different frames are computed
using multiple CPU cores and GPU.


