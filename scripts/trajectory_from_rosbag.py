#!/usr/bin/env python3.7

import rospy
from geometry_msgs.msg import PoseStamped
import argparse


def callback(data):
    pos = data.pose.position
    rot = data.pose.orientation

    timestamp = data.header.stamp.secs + (data.header.stamp.nsecs / 1000000000.0)
    print('{:f}'.format(timestamp) + " " + str(pos.x) + " " + str(pos.y) + " " + str(pos.z) + " " + str(rot.x) + " " +
          str(rot.y) + " " + str(rot.z) + " " + str(rot.w))


def listener(topic):

    rospy.init_node('pose_listener', anonymous=True)
    rospy.Subscriber(topic, PoseStamped, callback)
    rospy.spin()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Subscribe to PoseStamped topic and output CSV formatted pose.')
    parser.add_argument('--topic', '-t', help='Topic name', dest='topic', required=True, type=str)
    args = parser.parse_args()

    listener(args.topic)
