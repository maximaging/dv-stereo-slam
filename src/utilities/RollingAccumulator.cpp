//
// Created by rokas on 2021-01-21.
//

#include <dv-slam/utilities/RollingAccumulator.h>

#include <iostream>

slam::RollingAccumulator::RollingAccumulator(int64_t timelimit, size_t n, size_t s, const cv::Size& dims) :
        slam::SafeEventStore(timelimit),
        N(n),
        S(s),
        N_2(n / 2LL),
        accumulator(dims, 0.25f)
{}

std::vector<slam::StampedMat> slam::RollingAccumulator::accumulate() {
    std::vector<StampedMat> frames;
    size_t currentLength = this->getLength();
    if (currentLength >= N) {
        if (seekTime == 0LL) {
            seekTime = getLowestTime();
        }
        while (true) {
            dv::EventStore events = sliceTime(seekTime);
            size_t minEvents = std::max(N, S);
            if (events.size() >= minEvents) {
                dv::EventStore slice;
                if (S > N) {
                    slice = events.slice(0, S);
                    seekTime = slice.back().timestamp();
                    slice = slice.slice(0, N);
                } else {
                    slice = events.slice(0, N);
                    seekTime = slice.slice(0, S).back().timestamp();
                }
                accumulator.accumulate(slice);
                cv::Mat frameFloat = accumulator.generateFrame().image;
                cv::Mat frameU8(frameFloat.size(), CV_8UC1);
                frameFloat.convertTo(frameU8, CV_8UC1, 255.);
                int64_t timestamp = slice.slice(N_2, 1).front().timestamp();
                frames.emplace_back(timestamp, slice.front().timestamp(), slice.back().timestamp(), frameU8);
            } else {
                break;
            }
        }
    }
    return frames;
}

slam::StampedMat slam::RollingAccumulator::accumulateWithin(int64_t start, int64_t stop) {
    cv::Mat frameU8(accumulator.getShape(), CV_8UC1);
    if (this->isEmpty()) {
        frameU8 = 0;
        return {start, start, start, frameU8};
    }

    if (start < this->getLowestTime()) {
        start = this->getLowestTime();
    }
    if (stop > this->getHighestTime()) {
        stop = this->getHighestTime();
    }
    dv::EventStore slice = this->sliceTime(start, stop);
    if (slice.isEmpty()) {
        frameU8 = 0;
        return {start, start, start, frameU8};
    }

    accumulator.accumulate(slice);
    cv::Mat frameFloat = accumulator.generateFrame().image;
    frameFloat.convertTo(frameU8, CV_8UC1, 255.);
    int64_t timestamp = slice.slice(slice.getTotalLength() / 2, 1).front().timestamp();
    return {timestamp, slice.front().timestamp(), slice.back().timestamp(), frameU8};
}
