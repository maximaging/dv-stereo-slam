//
// Created by rokas on 2020-10-22.
//

#include <dv-slam/utilities/SafeEventStore.h>
#include <iostream>

void slam::SafeEventStore::safeAdd(const dv::EventStore &evts) {
    std::lock_guard<std::mutex> guard(myMutex);
    addEvents(evts);
}

dv::EventStore slam::SafeEventStore::safeSlice(const int64_t start, const int64_t end) {
    std::lock_guard<std::mutex> guard(myMutex);
    return sliceTime(start, end);
}

dv::EventStore slam::SafeEventStore::safeSlice(const int64_t start, const int64_t end, size_t& retStart, size_t& retEnd) {
    std::lock_guard<std::mutex> guard(myMutex);
    return sliceTime(start, end, retStart, retEnd);
}

void slam::SafeEventStore::addEvents(const dv::EventStore& evts) {
    dv::EventStore::add(evts);
    // Keep size within limits
    if (timelimit > 0LL) {
        while (this->getHighestTime() - this->getLowestTime() > timelimit) {
            size_t frontOffset = partialOffsets_.front();
            totalLength_ -= frontOffset;
            partialOffsets_.erase(partialOffsets_.begin());
            for (auto& offset : partialOffsets_) {
                offset -= frontOffset;
            }
            dataPartials_.erase(dataPartials_.begin());
        }
    }
}

slam::SafeEventStore::SafeEventStore(int64_t timelimit_us) : timelimit(timelimit_us) {}

dv::EventStore slam::SafeEventStore::sliceEvents(int64_t start, int64_t end) {
    return dv::EventStore::sliceTime(start, end);
}

dv::EventStore slam::SafeEventStore::sliceEvents(int64_t start, int64_t end, size_t &retStart, size_t &retEnd) {
	return dv::EventStore::sliceTime(start, end, retStart, retEnd);
}

int64_t slam::SafeEventStore::getStartTime() const {
	return dv::EventStore::getLowestTime();
}

int64_t slam::SafeEventStore::getEndTime() const {
	return dv::EventStore::getHighestTime();
}

size_t slam::SafeEventStore::getLength() const {
	return dv::EventStore::getTotalLength();
}
