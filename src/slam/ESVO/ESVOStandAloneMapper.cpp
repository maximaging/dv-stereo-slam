//
// Created by rokas on 2020-10-05.
//

#include <dv-slam/slam/ESVO/ESVOStandAloneMapper.h>

#include <dv-slam/utilities/Utils.h>

#include <pcl/filters/voxel_grid.h>
#include <random>
#include <utility>
#include <dv-slam/utilities/TimeProfiler.h>


#define ESVO_CORE_MAPPING_DEBUG 0

using namespace esvo_core;
using namespace esvo_core::core;

double ESVOStandAloneMapper::invDepth_max_range_ = 1. / 0.5;
double ESVOStandAloneMapper::invDepth_min_range_ = 1. / 6.5;

ESVOStandAloneMapper::ESVOStandAloneMapper(std::string calibration) :
        calibInfoDir_(std::move(calibration)),
        camSysPtr_(new CameraSystem(calibInfoDir_, false)),
        events_left_(3000000),
        ebm_(camSysPtr_, NUM_THREAD_MAPPING, false) {
    setMinInvDepth(0.15);
    setMaxInvDepth(2.0);
    residual_vis_threshold_ = 20;
    stdVar_vis_threshold_ = 0.15;
    age_max_range_ = 10;
    age_vis_threshold_ = 1;
    // module parameters
    TS_HISTORY_LENGTH_ = 100;
    // Event Block Matching (BM) parameters
    BM_half_slice_thickness_ = 2000LL;
    BM_patch_size_X_ = 15;
    BM_patch_size_Y_ = 7;
    patch_area_ = BM_patch_size_X_ * BM_patch_size_Y_;
    cost_vis_threshold_ = pow(residual_vis_threshold_, 2.) * static_cast<double>(patch_area_);
    BM_ZNCC_Threshold_ = 0.2;
    BM_bUpDownConfiguration_ = false;

    // SGM parameters (Used by Initialization)
    num_disparities_ = 16 * 8;
    block_size_ = 11;
    P1_ = 8 * 1 * block_size_ * block_size_;
    P2_ = 32 * 1 * block_size_ * block_size_;
    uniqueness_ratio_ = 11;

    BM_step_ = 1;

#if CUDA_STEREO_BM
    disparityScale = sgm::StereoSGM::SUBPIXEL_SCALE;

    // Dry run initialization
    auto cuda_sgbm = sgm::LibSGMWrapper(num_disparities_, 10, 120, 0.95f, true,
                                        sgm::PathType::SCAN_8PATH, BM_min_disparity_);

    cv::Mat output;
    cv::Mat input = cv::Mat::zeros(camSysPtr_->cam_left_ptr_->height_, camSysPtr_->cam_left_ptr_->width_, CV_8UC1);
    cuda_sgbm.execute(input, input, output);
#else
    sgbm_ = cv::StereoSGBM::create(0, num_disparities_, block_size_, P1_, P2_,
                                   -1, 0, uniqueness_ratio_);
    disparityScale = 16.0;
#endif
    dpConfigPtr_ = std::make_shared<DepthProblemConfig>(
            BM_patch_size_X_,
            BM_patch_size_Y_,
            "Tdist",
            2.1897, 16.6397, 1
    );
    dpSolver_ = std::make_shared<DepthProblemSolver>(camSysPtr_, dpConfigPtr_, NUMERICAL, NUM_THREAD_MAPPING);

    ESVO_System_Status_ = INITIALIZATION;

    updateEventBMParameters();
}

dv::EventStore ESVOStandAloneMapper::extractEvents(int64_t timestamp, int64_t thickness, size_t &eventCount) {
    dv::EventStore slice = events->safeSlice(timestamp - thickness, timestamp);
    eventCount = slice.size();
    return slice;
}


slam::SparseDepthFramePtr ESVOStandAloneMapper::InitializationAtTime(StampedTimeSurfaceObs &TS_obs_) {
    slam::SparseDepthFramePtr output = std::make_shared<slam::SparseDepthFrame>();
    output->timestamp = TS_obs_.first;
    slam::DepthEventsPtr vEMP = sparseDepthEstimation(TS_obs_, output->disparity);
    if (!vEMP) {
        return nullptr;
    }
#if ESVO_CORE_MAPPING_DEBUG
    std::cout << "********** Initialization (SGM) returns " << vEMP->size() << " points." << std::endl;
#endif
    double var_SGM = stdVar_vis_threshold_ * stdVar_vis_threshold_ * 0.95;

    output->depthPoints = std::make_shared<std::vector<DepthPoint>>();
    for (const auto &emp : *vEMP) {
        output->depthPoints->emplace_back(
                static_cast<size_t>(emp.x_left_(0)),
                static_cast<size_t>(emp.x_left_(1))
        );
        DepthPoint &dp = output->depthPoints->back();
        dp.update_x(emp.x_left_);
        Eigen::Vector3d p_cam;
        camSysPtr_->cam_left_ptr_->cam2World(emp.x_left_, emp.invDepth_, p_cam);
        dp.update_p_cam(p_cam);
        dp.update(emp.invDepth_, var_SGM);
        dp.residual() = 0.0;
        dp.age() = age_vis_threshold_;
        dp.updatePose(TS_obs_.second.tr_);
    }

    return output;
}

void ESVOStandAloneMapper::createEdgeMask(
        std::vector<dv::Event *> &vEventsPtr,
        PerspectiveCamera::Ptr &camPtr,
        cv::Mat &edgeMap,
        std::vector<std::pair<size_t, size_t> > &vEdgeletCoordinates,
        bool bUndistortEvents,
        size_t radius) {
    size_t col = camPtr->width_;
    size_t row = camPtr->height_;
    int dilate_radius = (int) radius;
    edgeMap = cv::Mat(cv::Size(col, row), CV_8UC1, cv::Scalar(0));
    vEdgeletCoordinates.reserve(col * row);

    auto it_tmp = vEventsPtr.begin();
    while (it_tmp != vEventsPtr.end()) {
        if (*it_tmp != nullptr) {
            // undistortion + rectification
            int xcoor, ycoor;
            // Avoid double precision operations if possible
            if (bUndistortEvents) {
                //Eigen::Matrix<double, 2, 1> coor;
                auto coor = camPtr->getRectifiedUndistortedCoordinate((*it_tmp)->x(), (*it_tmp)->y());
                xcoor = coor.x;
                ycoor = coor.y;
            } else {
                xcoor = (*it_tmp)->x();
                ycoor = (*it_tmp)->y();
            }

            // assign
            for (int dy = -dilate_radius; dy <= dilate_radius; dy++) {
                for (int dx = -dilate_radius; dx <= dilate_radius; dx++) {
                    int x = xcoor + dx;
                    int y = ycoor + dy;

                    if (x < 0 || x >= col || y < 0 || y >= row) {}
                    else {
                        edgeMap.at<uchar>(y, x) = 255;
                        vEdgeletCoordinates.emplace_back((size_t) x, (size_t) y);
                    }
                }
            }
        }
        it_tmp++;
    }
}

void ESVOStandAloneMapper::createEdgeCoords(
        PerspectiveCamera::Ptr &camPtr,
        std::vector<std::pair<size_t, size_t> > &vEdgeletCoordinates,
        bool bUndistortEvents) {
    int col = static_cast<int>(camPtr->width_);
    int row = static_cast<int>(camPtr->height_);
    vEdgeletCoordinates.reserve(vEventsPtr_left_SGM_.size());

    auto it_tmp = vEventsPtr_left_SGM_.begin();
    while (it_tmp != vEventsPtr_left_SGM_.end()) {
        // undistortion + rectification
        cv::Point2i coor;
        if (bUndistortEvents) {
            //Eigen::Matrix<double, 2, 1> coor;
            coor = camPtr->getRectifiedUndistortedCoordinate(
                    it_tmp->x(),
                    it_tmp->y()
            );
            if (coor.x < 0 || coor.x >= col || coor.y < 0 || coor.y >= row) {}
            else {
                vEdgeletCoordinates.emplace_back(coor.x, coor.y);
            }

        } else {
            vEdgeletCoordinates.emplace_back(it_tmp->x(), it_tmp->y());
        }
        it_tmp++;
    }
}

void ESVOStandAloneMapper::createEdgeCoords(
        const dv::EventStore &sliced_events,
        size_t event_count,
        PerspectiveCamera::Ptr &camPtr,
        std::vector<std::pair<size_t, size_t> > &vEdgeletCoordinates,
        bool bUndistortEvents) const {
    int col = static_cast<int>(camPtr->width_);
    int row = static_cast<int>(camPtr->height_);
    vEdgeletCoordinates.reserve(event_count > PROCESS_EVENT_NUM_ ? PROCESS_EVENT_NUM_ : event_count);

    auto it_tmp = sliced_events.begin();
    size_t count = 0;
    while (it_tmp != sliced_events.end()) {
        // undistortion + rectification
        cv::Point2i coor;
        if (bUndistortEvents) {
            //Eigen::Matrix<float, 2, 1> coor;
            coor = camPtr->getRectifiedUndistortedCoordinate(
                    it_tmp->x(),
                    it_tmp->y()
            );
            if (coor.x < 0 || coor.x >= col || coor.y < 0 || coor.y >= row) {}
            else {
                vEdgeletCoordinates.emplace_back(coor.x, coor.y);
                count++;
            }

        } else {
            vEdgeletCoordinates.emplace_back(it_tmp->x(), it_tmp->y());
            count++;
        }
        if (count >= PROCESS_EVENT_NUM_) {
            break;
        }
        it_tmp++;
    }
}

std::vector<std::pair<cv::Point2i, ESVOStandAloneMapper::EdgeInfo>> ESVOStandAloneMapper::createEdgeCoordsFromLeftImg(
        const StampedTimeSurfaceObs &TS_obs_,
        const cv::Mat &dispMap
) const {
    // Use pixels with above average intensity as edgepoints
    std::vector<std::pair<cv::Point2i, EdgeInfo>> edges;
    cv::Mat mask = TS_obs_.second.cvImagePtr_left_ > 126;
    std::vector<cv::Point2i> list;
    list.reserve(mask.total());
    for (int i = 0; i < mask.rows; i++) {
        for (int j = 0; j < mask.cols; j++) {
            if (mask.at<uint8_t>(i, j)) {
                list.emplace_back(j, i);
            }
        }
    }
    // Stochastic sample
    std::shuffle(list.begin(), list.end(), std::default_random_engine(TS_obs_.first));
    size_t count = 0LL;
    double denom = camSysPtr_->cam_left_ptr_->P_(0, 0) * camSysPtr_->baseline_ * disparityScale;
    for (const auto &coor : list) {
        auto disparity = dispMap.at<short>(coor);
        if (disparity > 0) {
            double invDepth = static_cast<double>(disparity) / denom;
            if (invDepth < invDepth_min_range_ || invDepth > invDepth_max_range_) {}
            else {
                edges.emplace_back(coor, EdgeInfo(invDepth, TS_obs_.first));
                count++;
                if (count >= PROCESS_EVENT_NUM_) {
                    break;
                }
            }
        }
    }
    return edges;
}


std::vector<std::pair<cv::Point2i, ESVOStandAloneMapper::EdgeInfo> > ESVOStandAloneMapper::createEdgeCoordsMasked(
        const dv::EventStore &sliced_events,
        size_t event_count,
        PerspectiveCamera::Ptr &camPtr,
        const cv::Mat &dispMap,
        const StampedTimeSurfaceObs &TS_obs_,
        bool bUndistortEvents,
        bool compensateMotion) {
    int col = static_cast<int>(camPtr->width_);
    int row = static_cast<int>(camPtr->height_);
    std::vector<std::pair<cv::Point2i, ESVOStandAloneMapper::EdgeInfo> > edgeletes;
    edgeletes.reserve(event_count > PROCESS_EVENT_NUM_ ? PROCESS_EVENT_NUM_ : event_count);

    StampTransformationMap tfs;
    Transformation finalTF = TS_obs_.second.tr_;
    if (compensateMotion) {
        if (auto tfsOpt = getInterpolatedTransforms(sliced_events.getLowestTime(), sliced_events.getHighestTime())) {
            tfs = tfsOpt.get();
        } else {
            compensateMotion = false;
        }

        if (!compensateMotion) {
            std::cout << "Motion compensation impossible" << std::endl;
        }
    }

    // Start iterating at the end, to get the latest events to the frame
    auto it_tmp = sliced_events.end();
    it_tmp--;
    size_t count = 0;
    double denom = camPtr->P_(0, 0) * camSysPtr_->baseline_ * disparityScale;
    do {
        // undistortion + rectification
        cv::Point2i coor;
        if (bUndistortEvents) {
            //Eigen::Matrix<float, 2, 1> coor;
            coor = camPtr->getRectifiedUndistortedCoordinate(
                    static_cast<uint32_t>(it_tmp->x()),
                    static_cast<uint32_t>(it_tmp->y())
            );
            if (coor.x < 0 || coor.x >= col || coor.y < 0 || coor.y >= row || !camPtr->testUndistortionMask(coor)) {
            } else {
                auto disparity = dispMap.at<short>(coor);
                if (disparity > 0) {
                    double invDepth = static_cast<double>(disparity) / denom;
                    if (invDepth < invDepth_min_range_ || invDepth > invDepth_max_range_) {}
                    else {
                        if (compensateMotion) {
                            auto currentTf = StampTransformationMap_lower_bound(tfs, it_tmp->timestamp());
                            if (currentTf == tfs.end()) {
                                currentTf--;
                            }
                            Transformation deltaTF = currentTf->second.inverse() * finalTF;
                            Eigen::Vector3d worldPoint;
                            Eigen::Vector2d pixelPoint(coor.x, coor.y);
                            camPtr->cam2World(pixelPoint, invDepth, worldPoint);
                            Eigen::Vector3d compensated = deltaTF.inverseTransform(worldPoint);
                            camPtr->world2Cam(compensated, pixelPoint);
                            coor.x = pixelPoint.x();
                            coor.y = pixelPoint.y();
                            if (!camPtr->boundaryCheck(coor.x, coor.y)) {
                                it_tmp--;
                                continue;
                            }
                            disparity = dispMap.at<short>(coor);
                            if (disparity > 0) {
                                double newInvDepth = static_cast<double>(disparity) / denom;
                                if (invDepth < invDepth_min_range_ || invDepth > invDepth_max_range_) {}
                                else {
                                    invDepth = newInvDepth;
                                }
                            }
                        }
                        edgeletes.emplace_back(coor, EdgeInfo(invDepth, it_tmp->timestamp()));
                        count++;
                    }
                }
            }

        } else {
            coor = cv::Point2i(
                    static_cast<int32_t>(it_tmp->x()),
                    static_cast<int32_t>(it_tmp->y())
            );
            auto disparity = dispMap.at<short>(coor);
            if (disparity > 0) {
                double invDepth = static_cast<double>(disparity) / denom;
                if (invDepth < invDepth_min_range_ || invDepth > invDepth_max_range_) {}
                else {
                    edgeletes.emplace_back(coor, EdgeInfo(invDepth, it_tmp->timestamp()));
                    count++;
                }
            }
        }
        if (count >= PROCESS_EVENT_NUM_) {
            break;
        }
        it_tmp--;
    } while (it_tmp != sliced_events.begin());
    return edgeletes;
}

DepthEventsPtr ESVOStandAloneMapper::sparseDepthEstimation(StampedTimeSurfaceObs &TS_obs_, cv::Mat &dispMap) {
    size_t eventCount = 0;
    dv::EventStore slice = extractEvents(TS_obs_.first, 10LL * BM_half_slice_thickness_, eventCount);
    if (eventCount == 0 || eventCount < minimum_events_to_work) {
        return nullptr;
    }
    cv::Size dimensions = TS_obs_.second.cvImagePtr_left_.size();
    if (dispMap.empty()) {
        dispMap = cv::Mat(dimensions, CV_16SC1);
    }
#if CUDA_STEREO_BM
    auto src_pitch = static_cast<int>(TS_obs_.second.cvImagePtr_left_.step1());
    auto dst_pitch = static_cast<int>(dispMap.step1());
    auto input_depth_bits = static_cast<int>(TS_obs_.second.cvImagePtr_left_.elemSize1()) * 8;
    auto output_depth_bits = static_cast<int>(dispMap.elemSize1()) * 8;
    auto inout_type = sgm::EXECUTE_INOUT_HOST2HOST;

    auto param_ = sgm::StereoSGM::Parameters(10, 120, 0.95f, true, sgm::PathType::SCAN_8PATH, BM_min_disparity_);
    sgm::StereoSGM cuda_sgbm = sgm::StereoSGM(dimensions.width, dimensions.height, num_disparities_, input_depth_bits,
                                              output_depth_bits, src_pitch, dst_pitch, inout_type, param_);
    size_t kernelSize = 3;
    TS_obs_.second.GaussianBlurTS(kernelSize);
    cuda_sgbm.execute(TS_obs_.second.cvImagePtr_left_.data, TS_obs_.second.cvImagePtr_right_.data, dispMap.data);
#else
    // call SGM on the current Time Surface observation pair.
//    cv::Mat dispMap, dispMap8;
    sgbm_->compute(TS_obs_.second.cvImagePtr_left_, TS_obs_.second.cvImagePtr_right_, dispMap);
#endif
    // call SGM on the current Time Surface observation pair.
    StampTransformationMap st_map_;

    std::vector<std::pair<cv::Point2i, EdgeInfo>> edgeletes;
    if (useHotPixels) {
        edgeletes = createEdgeCoordsFromLeftImg(TS_obs_, dispMap);
    } else {
        edgeletes = createEdgeCoordsMasked(slice, eventCount, camSysPtr_->cam_left_ptr_, dispMap, TS_obs_, true, false);
        if (ESVO_System_Status_ == SYSTEM_WORKING) {
            if (auto tfs = getInterpolatedTransforms(slice.getLowestTime(), slice.getHighestTime())) {
                st_map_ = tfs.get();
            }
        }
    }

#if ESVO_CORE_MAPPING_DEBUG
    std::cout << "Amount of events used for sparse depth estimation: " << slice.getTotalLength() << std::endl;
    std::cout << "Lag between timesurface and processing time: " << slice.getLowestTime() - TS_obs_.first << std::endl;
#endif

    // Apply logical "AND" operation and transfer "disparity" to "invDepth".
    DepthEventsPtr vEMP = std::make_shared<std::vector<EventMatchPair>>();
    vEMP->reserve(edgeletes.size());

    auto tf = st_map_.begin();
    int64_t step = BM_half_slice_thickness_ / 20LL;
    for (auto &vEdgeletCoordinate : edgeletes) {
        if (ESVO_System_Status_ == SYSTEM_WORKING && !st_map_.empty()) {
            // Lookup virtual view if available
            int64_t diff = vEdgeletCoordinate.second.timestamp - tf->first;
            if (diff > step) {
                tf = StampTransformationMap_lower_bound(st_map_, vEdgeletCoordinate.second.timestamp);
            }
            if (tf != st_map_.end()) {
                vEMP->emplace_back(
                        vEdgeletCoordinate.first,
                        vEdgeletCoordinate.second.timestamp,
                        vEdgeletCoordinate.second.invDepth,
                        tf->second
                );
            }
        } else {
            // Use current view
            vEMP->emplace_back(
                    vEdgeletCoordinate.first,
                    vEdgeletCoordinate.second.timestamp,
                    vEdgeletCoordinate.second.invDepth,
                    TS_obs_.second.tr_
            );
        }
    }
    return vEMP;
}

SparseDepthFramePtr ESVOStandAloneMapper::SGM_MappingAtTime(StampedTimeSurfaceObs &TS_obs_) {
    SparseDepthFramePtr output = std::make_shared<SparseDepthFrame>();
    output->timestamp = TS_obs_.first;
    output->transform = TS_obs_.second.tr_;

#if ESVO_CORE_MAPPING_DEBUG
    Utils::TimeProfiler tp;
    tp.tic();
#endif

    output->depthEvents = sparseDepthEstimation(TS_obs_, output->disparity);
    if (!output->depthEvents) {
        return nullptr;
    }
#if ESVO_CORE_MAPPING_DEBUG
    std::cout << "Sparse depth estimation takes: " << tp.toc() << std::endl;
    std::cout << "********** Mapping (SGM) returns " << vEMP->size() << " points." << std::endl;
    tp.tic();
#endif

    output->depthPoints = std::make_shared<std::vector<DepthPoint>>();
    if (depthSmoothing) {
        dpSolver_->setNumThread(1);
        dpSolver_->solve(&*output->depthEvents, &TS_obs_, *output->depthPoints); // hyper-thread version
#if ESVO_CORE_MAPPING_DEBUG
        std::cout << "Nonlinear optimization takes: " << tp.toc() << std::endl;
    std::cout << "Nonlinear optimization returns: " << output->depthPoints->size() << " estimates." << std::endl;
    tp.tic();
#endif
        dpSolver_->pointCulling(*output->depthPoints, stdVar_vis_threshold_, cost_vis_threshold_, invDepth_min_range_,
                                invDepth_max_range_);
#if ESVO_CORE_MAPPING_DEBUG
        std::cout << "pointCulling takes: " << tp.toc() << std::endl;
   std::cout << "After culling, vdp.size: " << output->depthPoints->size() << std::endl;
#endif
    } else {
        double var_SGM = stdVar_vis_threshold_ * stdVar_vis_threshold_ * 0.95;
        for (const auto &depthEvent : *output->depthEvents) {
            output->depthPoints->emplace_back(std::floor(depthEvent.x_left_(1)), std::floor(depthEvent.x_left_(0)));
            DepthPoint &dp = output->depthPoints->back();

            dp.update_x(depthEvent.x_left_);
            Eigen::Vector3d p_cam;
            camSysPtr_->cam_left_ptr_->cam2World(depthEvent.x_left_, depthEvent.invDepth_, p_cam);
            dp.update_p_cam(p_cam);
            if (dpConfigPtr_->LSnorm_enum_ == DepthProblemConfig::L2) {
                dp.update(depthEvent.invDepth_, var_SGM);
            } else if (dpConfigPtr_->LSnorm_enum_ == DepthProblemConfig::TDIST) {
                double scale2_rho = var_SGM * (dpConfigPtr_->td_nu_ - 2) / dpConfigPtr_->td_nu_;
                dp.update_studentT(depthEvent.invDepth_, scale2_rho, var_SGM, dpConfigPtr_->td_nu_);
            } else {
                exit(-1);
            }
            dp.residual() = var_SGM;
            dp.updatePose(depthEvent.trans_);
        }
    }

    return output;
}

SparseDepthFramePtr ESVOStandAloneMapper::EBM_MappingAtTime(StampedTimeSurfaceObs &TS_obs_) {
    /************************************************/
    /************ set the new DepthFrame ************/
    /************************************************/
    SparseDepthFramePtr output = std::make_shared<SparseDepthFrame>();
    output->timestamp = TS_obs_.first;
    output->transform = TS_obs_.second.tr_;

    std::vector<EventMatchPair> vEMP;// the container that stores the result of BM.
    /****************************************************/
    /*************** Block Matching (BM) ****************/
    /****************************************************/

    // Denoising operations
    size_t count;
    output->events = extractEvents(TS_obs_.first, 10LL * BM_half_slice_thickness_, count);

    // Limit number of events
    if (count > PROCESS_EVENT_NUM_) {
        output->events = output->events.slice(0, PROCESS_EVENT_NUM_);
    }
    StampTransformationMap st_map_;
    if (auto tfsOpt = getInterpolatedTransforms(output->events.getLowestTime(), output->events.getHighestTime())) {
        st_map_ = tfsOpt.get();
    }
    // block matching
    EventBM ebm(camSysPtr_, 1, false, BM_patch_size_X_, BM_patch_size_Y_,
                BM_min_disparity_, BM_max_disparity_, BM_step_, BM_ZNCC_Threshold_, BM_bUpDownConfiguration_);

    ebm.createMatchProblem(&TS_obs_, &st_map_, output->events);
    ebm.match_all_SingleThread(vEMP);

    /**************************************************************/
    /*************  Nonlinear Optimization & Fusion ***************/
    /**************************************************************/
    // nonlinear optimization
    output->depthPoints = std::make_shared<std::vector<DepthPoint>>();
    dpSolver_->setNumThread(1);
    dpSolver_->solve(&vEMP, &TS_obs_, *output->depthPoints); // hyper-thread version
#if ESVO_CORE_MAPPING_DEBUG
    LOG(INFO) << "Nonlinear optimization returns: " << output->depthPoints->size() << " estimates.";
#endif
    dpSolver_->pointCulling(*output->depthPoints, stdVar_vis_threshold_, cost_vis_threshold_, invDepth_min_range_,
                            invDepth_max_range_);
    return output;
}

void ESVOStandAloneMapper::createDenoisingMask(
        const dv::EventStore &vAllEventsPtr,
        cv::Mat &mask,
        size_t row, size_t col) {
    cv::Mat eventMap;
    visualizor_.plot_eventMap(vAllEventsPtr, eventMap, row, col);
    cv::medianBlur(eventMap, mask, 3);
}

dv::EventStore ESVOStandAloneMapper::extractDenoisedEvents(
        const dv::EventStore &vCloseEventsPtr,
        cv::Mat &mask,
        size_t maxNum) {
    dv::EventStore vEdgeEventsPtr;
    size_t counter = 0;
    for (const auto &event : vCloseEventsPtr) {
        if (counter >= maxNum)
            break;
        int x = event.x();
        int y = event.y();
        if (mask.at<uchar>(y, x) == 255) {
            vEdgeEventsPtr.add(event);
            counter++;
        }
    }
    return vEdgeEventsPtr;
}

void ESVOStandAloneMapper::pushEvents(const std::shared_ptr<const dv::EventPacket> &eventsPtr) {
    std::lock_guard<std::mutex> lock(data_mutex_);

    // Half a second in microseconds
    static constexpr int64_t max_time_diff_before_reset_s = 500000LL;

    // check time stamp inconsistency
    if (!eventsPtr->elements.empty() && !events_left_.empty()) {
        int64_t stamp_first_event = eventsPtr->elements.front().timestamp();
        const int64_t dt = stamp_first_event - events_left_.back().timestamp();
        if (dt < 0 || std::abs(dt) >= max_time_diff_before_reset_s) {
            throw runtime_error(fmt::format(
                    "Inconsistent event timestamps detected <ESVOStandAloneMapper::pushEvents> (new: %d, old %d).",
                    stamp_first_event, events_left_.back().timestamp()));
        }
    }

    // add new ones and remove old ones
    std::copy(eventsPtr->elements.begin(), eventsPtr->elements.end(), std::back_inserter(events_left_));
}

boost::optional<StampedTimeSurfaceObs> ESVOStandAloneMapper::dataTransferring(int64_t timestamp) {
    std::lock_guard<std::mutex> guard(data_mutex_);
    transformQueue.consume_all([this](const std::pair<int64_t, std::shared_ptr<Transformation>> &transform) {
        tf_.pushTransform(transform.first, *transform.second);
    });
    auto TS_obs = TS_history_.find(timestamp);
    if (TS_obs != TS_history_.end()) {
        Transformation tr;
        switch (ESVO_System_Status_) {
            case INITIALIZATION: {
                tr.setIdentity();
                break;
            }
            case SYSTEM_WORKING: {
                if (!tf_.getTransformAt(timestamp, tr)) {
                    std::cout << "Mapper: Transformation is not available" << std::endl;
                    return boost::none;
                }
            }
        }
        TS_obs->second.setTransformation(tr);
        return StampedTimeSurfaceObs(*TS_obs);
    } else {
        std::cout << "Mapper: TimeSurface is not available" << std::endl;
    }
    return boost::none;
}

boost::optional<StampTransformationMap>
ESVOStandAloneMapper::getInterpolatedTransforms(int64_t startTime, int64_t endTime, boost::optional<int64_t> timestep) {
    StampTransformationMap transforms;
    int64_t tstep = timestep ? timestep.get() : BM_half_slice_thickness_ / 20LL;
    bool success = true;
    {
        std::lock_guard<std::mutex> guard(data_mutex_);
        Transformation tr;
        while (startTime < endTime) {
            if (!tf_.getTransformAt(startTime, tr)) {
                success = false;
                break;
            }
            transforms.emplace(startTime, tr);
            startTime += tstep;
        }
    }
    if (success && !transforms.empty()) {
        return transforms;
    } else {
        return boost::none;
    }
}

void ESVOStandAloneMapper::pushFramePair(cv::Mat &left, cv::Mat &right, int64_t t_new_TS) {
    std::lock_guard<std::mutex> guard(data_mutex_);

    // push back the new time surface map
    // Made the gradient computation optional which is up to the jacobian choice.
    bool calcGradient = dpSolver_->getProblemType() == ANALYTICAL;
    TS_history_.emplace(t_new_TS, TimeSurfaceObservation(
            left, right, TS_id_, calcGradient
    ));
    TS_id_++;

    // keep TS_history's size constant
    while (TS_history_.size() > TS_HISTORY_LENGTH_) {
        auto it = TS_history_.begin();
        TS_history_.erase(it);
    }
}

cv::Mat ESVOStandAloneMapper::getDepthFrameImage(const cv::Mat &background, const DepthFrame::Ptr &map) {
    cv::Mat invDepthImage;
    if (!background.empty()) {
        background.copyTo(invDepthImage);
    }
    if (!map->dMap_->empty()) {
        Visualization::plot_map(map->dMap_, tools::InvDepthMap, invDepthImage,
                                ESVOStandAloneMapper::invDepth_max_range_, ESVOStandAloneMapper::invDepth_min_range_,
                                0.15);
    }
    return invDepthImage;
}

void ESVOStandAloneMapper::pushTransformation(const Transformation &tf, int64_t t_new_TS) {
    transformQueue.push(std::make_pair(t_new_TS, std::make_shared<Transformation>(tf)));
}

bool ESVOStandAloneMapper::isDepthEstimationOnly() const {
    return depthEstimationOnly;
}

void ESVOStandAloneMapper::setDepthEstimationOnly(bool depthEstimationOnly) {
    ESVOStandAloneMapper::depthEstimationOnly = depthEstimationOnly;
}

void ESVOStandAloneMapper::setMinimumEventsToWork(size_t minimumEventsToWork) {
    minimum_events_to_work = minimumEventsToWork;
}

SparseDepthFramePtr ESVOStandAloneMapper::MappingAtTime(StampedTimeSurfaceObs &TS_obs_) {
    if (ESVO_System_Status_ == SYSTEM_WORKING) {
        if (useEventBlockMatcher) {
            return EBM_MappingAtTime(TS_obs_);
        } else {
            return SGM_MappingAtTime(TS_obs_);
        }
    } else {
        return InitializationAtTime(TS_obs_);
    }
}

bool ESVOStandAloneMapper::isUseEventBlockMatcher() const {
    return useEventBlockMatcher;
}

void ESVOStandAloneMapper::setUseEventBlockMatcher(bool useEventBlockMatcher_) {
    ESVOStandAloneMapper::useEventBlockMatcher = useEventBlockMatcher_;
}

void ESVOStandAloneMapper::setMinimumDepthPointsToInitialize(size_t initSgmDpNumThreshold) {
    INIT_SGM_DP_NUM_Threshold_ = initSgmDpNumThreshold;
}

void ESVOStandAloneMapper::setBmHalfSliceThickness(int64_t bmHalfSliceThickness) {
    BM_half_slice_thickness_ = bmHalfSliceThickness;
}

void ESVOStandAloneMapper::setEvents(const SafeEventStore::Ptr &events) {
    ESVOStandAloneMapper::events = events;
}

void ESVOStandAloneMapper::reset() {
    // clear all maintained data
    TS_history_.clear();
    tf_.clear();
    TS_id_ = 0;

    ebm_.resetParameters(BM_patch_size_X_, BM_patch_size_Y_,
                         BM_min_disparity_, BM_max_disparity_, BM_step_, BM_ZNCC_Threshold_, BM_bUpDownConfiguration_);
    ESVO_System_Status_ = INITIALIZATION;
}


void ESVOStandAloneMapper::setUseHotPixels(bool useHotPixels) {
    ESVOStandAloneMapper::useHotPixels = useHotPixels;
}

void ESVOStandAloneMapper::setMinInvDepth(double minInvDepth) {
    invDepth_min_range_ = minInvDepth;

    double f = (camSysPtr_->cam_left_ptr_->P_(0, 0) + camSysPtr_->cam_left_ptr_->P_(1, 1)) / 2.;
    double b = camSysPtr_->baseline_;
    min_disparity_ = max(static_cast<short>(std::floor(f * b * invDepth_min_range_)), BM_min_disparity_);
}

void ESVOStandAloneMapper::setMaxInvDepth(double maxInvDepth) {
    invDepth_max_range_ = maxInvDepth;

    double f = (camSysPtr_->cam_left_ptr_->P_(0, 0) + camSysPtr_->cam_left_ptr_->P_(1, 1)) / 2.;
    double b = camSysPtr_->baseline_;
    max_disparity_ = min(static_cast<short>(std::ceil(f * b * invDepth_max_range_)), BM_max_disparity_);
}

void ESVOStandAloneMapper::updateEventBMParameters() {
    // calculate the min,max disparity
    ebm_.resetParameters(BM_patch_size_X_, BM_patch_size_Y_, static_cast<size_t>(min_disparity_),
                         static_cast<size_t>(max_disparity_), BM_step_, BM_ZNCC_Threshold_, BM_bUpDownConfiguration_);
}

void ESVOStandAloneMapper::setStdVarVisThreshold(double stdVarVisThreshold) {
    stdVar_vis_threshold_ = stdVarVisThreshold;
}

void ESVOStandAloneMapper::setStatusToRunning() {
    ESVO_System_Status_ = SYSTEM_WORKING;
}

void ESVOStandAloneMapper::setDepthSmoothing(bool depthSmoothing) {
    ESVOStandAloneMapper::depthSmoothing = depthSmoothing;
}
