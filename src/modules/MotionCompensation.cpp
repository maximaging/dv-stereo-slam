//
// Created by rokas on 2020-10-28.
//

#include <functional>

#include <dv-sdk/module.hpp>
#include <dv-sdk/processing.hpp>

#include <better_flow/common.h>
#include <better_flow/dvs_flow.h>
#include <better_flow/opencl_driver.h>

#include <dv-slam/utilities/ExecutionTimer.h>

#define MC_PROFILING 0

#if MC_PROFILING
#include "utilities/TimeProfiler.h"
#endif

class MotionCompensation : public dv::ModuleBase {
private:
    std::shared_ptr<ExecutionTimer> timer = nullptr;
    dv::EventStore events;
    int64_t lastRunTime = 0ULL;
    cv::Size outputSize;
    int maxIters;
    int frequency;
    int64_t timestep;
    size_t maxEvents;

    ObjectModel lastModel;
    bool useFinal = false;

public:
    static void initInputs(dv::InputDefinitionList &in) {
        in.addEventInput("events");
    }

    static void initOutputs(dv::OutputDefinitionList &out) {
        out.addEventOutput("events");
        out.addFrameOutput("frame");
    }

    static const char *initDescription() {
        return ("Performs motion compensation on events to filter motion blur.");
    }

    static void initConfigOptions(dv::RuntimeConfig &config) {
        config.add("target_frequency",
                   dv::ConfigOption::intOption("Target processing frequency", 100, 1, 500));
        config.add("max_events",
                   dv::ConfigOption::intOption("Maximum number of events per iteration", 15000, 1000, 50000));
        config.add("max_iterations",
                   dv::ConfigOption::intOption("Maximum number of optimization iterations, lower values gives more deterministic execution time", 10, 1, 100));
#ifdef OPENCL_KERNEL_FILE
        config.add("gpu_acceleration",
                   dv::ConfigOption::boolOption("Enable GPU acceleration", false));
#endif
        config.setPriorityOptions({"target_frequency", "max_events"});
    }

    void configUpdate() override {
        timer = nullptr;
        maxIters = config.getInt("max_iterations");
        frequency = config.getInt("target_frequency");
        timestep = static_cast<int64_t>(1e+6 / (frequency * 10));
        maxEvents = static_cast<size_t>(config.getInt("max_events"));
#ifdef OPENCL_KERNEL_FILE
        if (config.getBool("gpu_acceleration") && !OpenCLDriver::enabled) {
            OpenCLDriver::init(OPENCL_KERNEL_FILE);
        } else {
            OpenCLDriver::enabled = false;
        }
#endif
    }

    MotionCompensation() {
#ifdef OPENCL_KERNEL_FILE
        OpenCLDriver::init(OPENCL_KERNEL_FILE);
#endif
        if (!OpenCLDriver::enabled) {
            log.warning("GPU Support is unavailable");
        }

        auto frameIn = inputs.getFrameInput("events");
        outputs.getEventOutput("events").setup(frameIn.sizeX(), frameIn.sizeY(), frameIn.getOriginDescription());
        outputs.getFrameOutput("frame").setup(frameIn.sizeX(), frameIn.sizeY(), frameIn.getOriginDescription());
        outputSize = cv::Size(frameIn.sizeX(), frameIn.sizeY());
    };

    void motionCompensation(const dv::EventStore& slice) {
        // Perform motion compensation
        LinearEventCloud data;

        OptimizerRolling<LinearEventCloud> optimizer;
        for (const auto& ev : slice) {
            Event event(static_cast<uint>(ev.x()), static_cast<uint>(ev.y()), static_cast<ull>(ev.timestamp()));
            event.valid = ev.polarity();
            data.push_back(event);
        }

        optimizer.set_cloud(&data, 1.0);
        optimizer.set_time(data.begin()->timestamp);
        optimizer.set_maxiter(maxIters);

        optimizer.set_model(lastModel);
        if(optimizer.run()) {
            log.warning("Failed to detect motion.");
            return;
        }
        ObjectModel model = optimizer.get_model();
        if (model.cnt == 0 || isnan(model.cx)) {
            log.warning("Optimizer fails to reconstruct output events. Consider raising maximum events for motion compensation.");
            return;
        } else {
            lastModel = model;
        }
        // Sometime it's good for debug
//        std::cout << "-----------------" << std::endl;
//        std::cout << lastModel << std::endl << std::endl;

        // Generate output
        cv::Mat outFrame = cv::Mat::zeros(outputSize, CV_8UC1);
        std::vector<dv::Event> output;
        for (auto& mc_event : data) {
            mc_event.compute_uv();
            if (!mc_event.noise) {
                int x, y;
                if (useFinal) {
                    x = static_cast<int>(std::round(mc_event.best_pr_x));
                    y = static_cast<int>(std::round(mc_event.best_pr_y));
                } else {
                    x = static_cast<int>(std::round(mc_event.pr_x));
                    y = static_cast<int>(std::round(mc_event.pr_y));
                }
                output.emplace_back(mc_event.timestamp, x, y, mc_event.valid);
                outFrame.at<uint8_t>(y, x)++;
            }
        }
        auto evout = outputs.getEventOutput("events");
        evout << output << dv::commit;
        auto frameOutput = outputs.getFrameOutput("frame");
        double img_scale = 127.0 / cv::mean(outFrame, outFrame > 0)[0];
        frameOutput << slice.getLowestTime() << outFrame * img_scale << dv::commit;
    }

    void run() override {
        auto evinput = inputs.getEventInput("events");
        int64_t start = 0LL;
        int64_t stop;
        if (auto input = evinput.events()) {
            events.add(dv::EventStore(input));
            start = input.front().timestamp();
            if (timer == nullptr) {
                timer = std::make_shared<ExecutionTimer>(start, frequency);
                lastRunTime = start;
            }
            stop = input.back().timestamp();
            while (start <= stop) {
                if (timer->timeToRun(start)) {
#if MC_PROFILING
                    Utils::TimeProfiler tt;
                    tt.tic();
#endif
                    size_t startIndex, endIndex;
                    auto slice = events.sliceTime(lastRunTime, start, startIndex, endIndex);
                    size_t eventAmount = endIndex - startIndex;
                    if (eventAmount >= 1000) {
                        if (eventAmount > maxEvents) {
                            auto decimatedSlice = slice.slice(0, maxEvents);
                            motionCompensation(decimatedSlice);
                        } else {
                            motionCompensation(slice);
                        }
                        lastRunTime = start;
                    }
#if MC_PROFILING
                    static double total_sum = 0.0;
                    static uint64_t counter = 0;
                    counter += 1;
                    total_sum += tt.toc();
                    if (counter >= 50) {
                        double avg = total_sum / (double) counter;
                        total_sum = 0.0;
                        counter = 0;
                        std::cout << avg << std::endl;
                    }
#endif
                }
                start += timestep;
            }
        }
    }
};

registerModuleClass(MotionCompensation)
