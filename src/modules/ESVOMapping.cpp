//
// Created by rokas on 2020-10-05.
//

#include <functional>

#include <dv-sdk/module.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/filesystem.hpp>

#include <pcl/io/pcd_io.h>
#ifdef WITH_OCTOMAP
#include <octomap/octomap.h>
#endif

#include <dv-slam/utilities/ExecutionTimer.h>
#include <dv-slam/slam/ESVO/ESVOStandAloneMapper.h>
#include <dv-slam/slam/ESVO/ESVOStandAloneTracker.h>
#include <dv-slam/utilities/SafeQueue.h>
#include <dv-slam/utilities/TimeProfiler.h>
#include <dv-slam/utilities/SafeEventStore.h>
#include <dv-slam/slam/ESVO/ESVOFuser.h>
#include <dv-slam/utilities/MultiQueue.h>
#include <dv-slam/utilities/RollingAccumulator.h>

#include <dv-processing/data/pose_base.hpp>

namespace fs = boost::filesystem;

class ESVOMapping : public dv::ModuleBase {
    typedef dv::InputDataWrapper<dv::Frame> DvFrame;
    typedef boost::circular_buffer<DvFrame> FrameBuffer;
private:
    std::unique_ptr<ESVOStandAloneMapper> mapper = nullptr;
    std::unique_ptr<ESVOStandAloneTracker> tracker = nullptr;
    std::unique_ptr<slam::ESVOFuser> fuser = nullptr;
//    FrameBuffer left, right;
    std::unique_ptr<slam::RollingAccumulator> leftEvents = nullptr;
    std::unique_ptr<slam::RollingAccumulator> rightEvents = nullptr;
    boost::circular_buffer<slam::StampedMat> leftBuffer = boost::circular_buffer<slam::StampedMat>(20);

    int64_t eventHalfSlice = 2000LL;

    // Buffer these to avoid memory reallocations
    ExecutionTimer mappingTimer;
    ExecutionTimer pcWriteTimer = ExecutionTimer(0LL, 1);

    std::atomic<bool> initialized = false;
    std::atomic<bool> writingNow = false;
    std::atomic<bool> writingPCDNow = false;

    SafeQueue<int64_t> trackingRunQueue;
    MultiQueue<int64_t> mappingRunQueue;
    SafeQueue<slam::SparseDepthFramePtr> fusionRunQueue;

    boost::circular_buffer<int64_t> mappingTimestamps = boost::circular_buffer<int64_t>(2);

    Utils::TimeProfiler trackingProfiler, fusingProfiler, mappingProfiler;
    SafeEventStore::Ptr events;
    size_t minimumEvents = 2000;
    bool debugMode = false;

    std::string outputDirectory;
    std::unique_ptr<std::ofstream> trajectoryOut = nullptr;
    std::string calibration;
    int64_t lastProcessTime = 0;

    std::map<int64_t, cv::Mat> rectifiedLeft;

#ifdef WITH_OCTOMAP
    octomap::OcTree octree = octomap::OcTree(0.1);
#endif

public:
    static void initInputs(dv::InputDefinitionList &in) {
        in.addEventInput("left");
        in.addEventInput("right");
        in.addEventInput("events");
    }

    static void initTypes(std::vector<dv::Types::Type> &types) {
        types.push_back(dv::Types::makeTypeDefinition<dv::Pose, dv::Pose>("A point to be plotted"));
    }

    static void initOutputs(dv::OutputDefinitionList &out) {
        out.addFrameOutput("disparity");
        out.addFrameOutput("depth");
        out.addFrameOutput("reprojection_map");
        out.addFrameOutput("left");
        out.addFrameOutput("right");
        out.addOutput("pose", dv::Pose::TableType::identifier);
        out.addEventOutput("events");
    }

    static void initConfigOptions(dv::RuntimeConfig &config) {
        config.add("mapping_frequency",
                   dv::ConfigOption::intOption("Target mapping frequency", 15, 0, 50));
        config.add("depth_only",
                   dv::ConfigOption::boolOption("Perform sparse depth estimation only.", false));
        config.add("use_event_block_matcher",
                   dv::ConfigOption::boolOption("Estimate depth using event block matcher.", false));
        config.add("debug_mode",
                   dv::ConfigOption::boolOption("Enable generation of visualizations.", false));
        config.add("minimum_events_to_estimate",
                   dv::ConfigOption::intOption("Set minimum number of event coming to perform any estimation.", 200, 0,
                                               10000));
        config.add("min_depth_points_to_initialize",
                   dv::ConfigOption::intOption(
                           "Set minimum number of depth points that must estimated to initialize SLAM.", 500, 0,
                           10000));
        config.add("sparse_depth_point_count",
                   dv::ConfigOption::intOption(
                           "Set number of depth points are used for motion estimation.", 2000, 0,
                           50000));
//        config.add("use_hot_pixels",
//                   dv::ConfigOption::boolOption("Use hot pixels instead of events for sparse depth point selection",
//                                                false));
        config.add("event_half_slice",
                   dv::ConfigOption::intOption(
                           "Set time slicing interval in microseconds, that used to select events for sparse depth estimation.",
                           2000, 100, 100000));
        config.add("temporal_window_size",
                   dv::ConfigOption::intOption(
                           "Set time slicing interval in microseconds, that used to select events for sparse depth estimation.",
                           10000, 2, 100000));
        config.add("temporal_window_step",
                   dv::ConfigOption::intOption(
                           "Set time slicing interval in microseconds, that used to select events for sparse depth estimation.",
                           3000, 1, 100000));
        config.add("global_map_output",
                   dv::ConfigOption::directoryOption("Choose a directory where output results are going to be saved",
                                                     ""));
        config.add("min_depth",
                   dv::ConfigOption::doubleOption("Minimum depth to estimate", 0.5, 0.1, 250.0));
        config.add("max_depth",
                   dv::ConfigOption::doubleOption("Maximum depth to estimate", 6.25, 0.1, 250.0));
        config.add("stdvar_threshold",
                   dv::ConfigOption::doubleOption("Standard deviation threshold to be used to get more sparser, but more reliable depth map", 0.15, 0.0, 1.0));
        config.add("calibration",
                   dv::ConfigOption::fileOpenOption("Select stereo calibration"));
        config.add("tracker_contrast",
                   dv::ConfigOption::doubleOption("Additional contrasting performed on input image that can improve tracking on streams with low event count", 1.0, 1.0, 10.0));
        config.add("depth_smoothing",
                   dv::ConfigOption::boolOption("Enable depth smoothing using non-linear estimation", true));
        config.setPriorityOptions({"calibration", "event_half_slice", "use_event_block_matcher", "global_map_output"});
    }

    void configUpdate() override {
        if (!mapper || !tracker) {
            std::string calibrationFile = config.getString("calibration");
            if (calibrationFile.empty()) {
                throw std::runtime_error("Please provide stereo calibration location");
            } else {
                std::string extension = fs::extension(calibrationFile);
                std::transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
                if (extension != ".xml") {
                    calibrationFile = fs::path(calibrationFile).parent_path().string();
                }
                mapper = std::make_unique<ESVOStandAloneMapper>(calibrationFile);
                tracker = std::make_unique<ESVOStandAloneTracker>(calibrationFile);
                fuser = std::make_unique<slam::ESVOFuser>(mapper->camSysPtr_, mapper->dpConfigPtr_->LSnorm_enum_);
            }
        }
        auto frameIn = inputs.getEventInput("left");
        if (mapper->camSysPtr_->cam_left_ptr_->width_ != frameIn.sizeX() ||
            mapper->camSysPtr_->cam_left_ptr_->height_ != frameIn.sizeY()) {
            throw std::runtime_error("Input image dimensions does not match camera calibration!");
        }
        if (!mapper || !tracker) {
            throw std::runtime_error("Improper module configuration, please check stereo calibration");
        }
        mapper->setEvents(events);
        mapper->setDepthEstimationOnly(config.getBool("depth_only"));
        mapper->setUseEventBlockMatcher(config.getBool("use_event_block_matcher"));
        double stdvar_threshold = config.getDouble("stdvar_threshold");
        mapper->setStdVarVisThreshold(stdvar_threshold);
        fuser->setStdVarVisThreshold(stdvar_threshold);
        mappingTimer = ExecutionTimer(0LL, static_cast<uint32_t>(config.getInt("mapping_frequency")));
        debugMode = config.getBool("debug_mode");
        minimumEvents = static_cast<size_t>(config.getInt("minimum_events_to_estimate"));
        mapper->setMinimumEventsToWork(minimumEvents);
        mapper->setMinimumDepthPointsToInitialize(static_cast<size_t>(config.getInt("min_depth_points_to_initialize")));
        eventHalfSlice = static_cast<int64_t>(config.getInt("event_half_slice"));
        mapper->setBmHalfSliceThickness(eventHalfSlice);
        mapper->setDepthSmoothing(config.getBool("depth_smoothing"));
//        fuser->setPointCount(static_cast<size_t>(config.getInt("sparse_depth_point_count")));
//        mapper->setUseHotPixels(config.getBool("use_hot_pixels"));
        outputDirectory = config.getString("global_map_output");
        if (!outputDirectory.empty()) {
            trajectoryOut = std::make_unique<std::ofstream>(
                    outputDirectory + "/stamped_traj_estimate.txt",
                    std::ofstream::trunc
            );
        }
        double contrast = config.getDouble("tracker_contrast");
        tracker->setContrast(contrast);

        double minDepth = config.getDouble("min_depth");
        double maxDepth = config.getDouble("max_depth");

        if (maxDepth > minDepth) {
            double minInvDepth = 1.0 / maxDepth;
            double maxInvDepth = 1.0 / minDepth;
            mapper->setMinInvDepth(minInvDepth);
            mapper->setMaxInvDepth(maxInvDepth);
            tracker->setMinInvDepthRange(minInvDepth);
            tracker->setMaxInvDepthRange(maxInvDepth);
            fuser->setInvDepthMinRange(minInvDepth);
            fuser->setInvDepthMaxRange(maxInvDepth);
        } else {
            log.warning(fmt::format("Minimum estimated depth cannot be larger than maximum [{} >= {}]",
                                    minDepth, maxDepth));
        }
    }

    static const char *initDescription() {
        return "ESVO SLAM algorithm implementation.";
    }

    ESVOMapping() : trackingRunQueue(5), mappingRunQueue(4, 10), fusionRunQueue(50) {
//        left = boost::circular_buffer<DvFrame>(5);
//        right = boost::circular_buffer<DvFrame>(5);
        auto frameIn = inputs.getEventInput("left");
        int width = frameIn.sizeX();
        int height = frameIn.sizeY();
        size_t windowSize = static_cast<size_t>(config.getInt("temporal_window_size"));
        size_t windowStep = static_cast<size_t>(config.getInt("temporal_window_step"));
        // Keep a history of 2 seconds
        int64_t rollingTimeLimit = 2000000LL;

        leftEvents = std::make_unique<slam::RollingAccumulator>(rollingTimeLimit, windowSize, windowStep, cv::Size(width, height));
        rightEvents = std::make_unique<slam::RollingAccumulator>(rollingTimeLimit, windowSize, windowStep, cv::Size(width, height));
        outputs.getFrameOutput("disparity").setup(width, height, frameIn.getOriginDescription());
        outputs.getFrameOutput("depth").setup(width, height, frameIn.getOriginDescription());
        outputs.getFrameOutput("reprojection_map").setup(width, height, frameIn.getOriginDescription());
        outputs.getFrameOutput("left").setup(width, height, frameIn.getOriginDescription());
        outputs.getFrameOutput("right").setup(width, height, frameIn.getOriginDescription());
        outputs.getEventOutput("events").setup(width, height, frameIn.getOriginDescription());
        outputs.getOutput<dv::Pose>("pose").setup(frameIn.getOriginDescription());

        events = std::make_shared<SafeEventStore>(rollingTimeLimit);

        fusionRunQueue.autoDequeueAsync([&] (const slam::SparseDepthFramePtr& depthFrame) {
            fusingProfiler.tic();
            auto depthFused = fuser->fuse(depthFrame);
            if (!depthFused) {
                return;
            }

            auto minNumPoints = static_cast<size_t>(config.getInt("min_depth_points_to_initialize"));
            if (mapper->isDepthEstimationOnly()) {
                fuser->reset();
            } else if (depthFused->dMap_->size() > minNumPoints) {
                tracker->pushReferencePointCloud(depthFused->getPointCloud(), depthFrame->timestamp);
                if (!initialized) {
                    mappingRunQueue.reset();
                    mapper->setStatusToRunning();
                    initialized = true;
                }
                fusingProfiler.toc();

                if (!outputDirectory.empty() && pcWriteTimer.timeToRun(depthFrame->timestamp)) {
                    auto pc = fuser->updateGlobalPointCloud(depthFused);
                    // Hard copy the point cloud to pass saving in another thread
                    if (!pc->empty() && !writingPCDNow) {
                        writingPCDNow = true;
                        std::thread([&, pc]() {
                            pcl::io::savePCDFileBinary(outputDirectory + "/point_cloud.pcd", *pc);
                            writingPCDNow = false;
                        }).detach();
                    }
#ifdef WITH_OCTOMAP

                    if (!writingNow) {
                        writingNow = true;
                        std::thread([&, depthFused]() {
                            octomap::Pointcloud opc;
                            for (const auto &dp : *depthFused->dMap_) {
                                const auto &point = dp.p_cam();
                                opc.push_back(octomap::point3d(
                                        static_cast<float>(point.x()),
                                        static_cast<float>(point.y()),
                                        static_cast<float>(point.z())
                                ));
                            }
                            auto position = depthFused->T_world_frame_.getPosition();
                            octomap::point3d cameraPose(
                                    static_cast<float>(position.x()),
                                    static_cast<float>(position.y()),
                                    static_cast<float>(position.z())
                            );
                            auto quat = depthFused->T_world_frame_.getEigenQuaternion();
                            octomath::Quaternion oQuat(quat.w(), quat.x(), quat.y(), quat.z());
                            octomap::pose6d framePose(cameraPose, oQuat);
                            octree.insertPointCloud(opc, octomap::point3d(0, 0, 0), framePose);

                            octree.write(outputDirectory + "/map.ot");
                            writingNow = false;
                        }).detach();
                    }
#endif
                }
//                std::cout << "% Fusion: " << fusingProfiler << std::endl;
            } else {
                log.warning << "Not enough depth points to initialize visual odometry, estimated "
                            << depthFused->dMap_->size() << " points out of " << minNumPoints
                            << " required. Consider reducing the value of MIN_DEPTH_POINTS_TO_INITIALIZE parameter.";
            }
            if (debugMode) {
                cv::Mat depthImage = ESVOStandAloneMapper::getDepthFrameImage(cv::Mat(), depthFused);
                if (!depthImage.empty()) {
                    auto depthOut = outputs.getFrameOutput("depth");
                    depthOut << depthFrame->timestamp << depthImage << dv::commit;
                }
            }
        });

        mappingRunQueue.autoDequeue([&](int64_t timestamp) {
            mappingProfiler.tic();
            if (auto tsobs = mapper->dataTransferring(timestamp)) {
                auto TS_obs_ = tsobs.get();
                if (auto depthFrame = mapper->MappingAtTime(TS_obs_)) {
                    fusionRunQueue.enqueue(depthFrame);
                    if (debugMode) {
                        if (depthFrame->depthEvents && !depthFrame->depthEvents->empty()) {
                            std::vector<dv::Event> eventOutput;
                            eventOutput.reserve(depthFrame->depthEvents->size());
                            for (const auto& depthEvent : *depthFrame->depthEvents) {
                                eventOutput.emplace_back(
                                        depthEvent.t_,
                                        depthEvent.x_left_(0),
                                        depthEvent.x_left_(1),
                                        true);
                            }
                            outputs.getEventOutput("events") << eventOutput << dv::commit;
                        }
                        auto disparityOut = outputs.getFrameOutput("disparity");
                        cv::Mat disparityPreview;
                        depthFrame->disparity.convertTo(disparityPreview, CV_8UC1, 255. / (16.0 * 8.0 * 16.0));
                        disparityOut << depthFrame->timestamp << disparityPreview << dv::commit;
                    }
                } else {
                    log.warning << "Mapping fails";
                }
                mappingProfiler.toc();
                if (debugMode) {
                    outputs.getFrameOutput("left") << tsobs->second.cvImagePtr_left_ << dv::commit;
                    outputs.getFrameOutput("right") << tsobs->second.cvImagePtr_right_ << dv::commit;
                }
//                std::cout << "# Mapper: " << mappingProfiler << std::endl;
            } else {
                log.warning << "Failed to transfer data into mapping thread";
            }
        });

        trackingRunQueue.autoDequeueAsync([&](int64_t timestamp) {
            if (minimumEvents > 0 && events->safeSlice(timestamp - (10LL * eventHalfSlice), timestamp).getTotalLength() < minimumEvents) {
                return;
            }
            trackingProfiler.tic();
            tracker->refDataTransferring();
            if (tracker->curDataTransferring(timestamp)) {
                if (tracker->track()) {
                    auto myPos = tracker->getWorldTransform();
                    mapper->pushTransformation(myPos.second, myPos.first);
                    if (debugMode) {
                        auto reprojectionOut = outputs.getFrameOutput("reprojection_map");
                        reprojectionOut << timestamp << tracker->visualizeTracking() << dv::commit;
                    }
                    auto position = myPos.second.getPosition();
                    auto rotation = myPos.second.getRotation();
                    if (trajectoryOut) {
                        *trajectoryOut << std::fixed << std::setprecision(6)
                                       << static_cast<double>(myPos.first) / 1.0e+6 << " "
                                       << position.x() << " " << position.y() << " " << position.z() << " "
                                       << rotation.x() << " " << rotation.y() << " " << rotation.z() << " "
                                       << rotation.w() << std::endl;
                    }
                    auto poseMsg  = outputs.getOutput<dv::Pose>("pose").data();

                    poseMsg->timestamp = myPos.first;
                    poseMsg->translation = dv::Vec3f(position.x(), position.y(), position.z());
                    poseMsg->rotation = dv::Quaternion(rotation.w(), rotation.x(), rotation.y(), rotation.z());

                    poseMsg.commit();
                } else {
                    std::cout <<"Unable to perform tracking, since there is not enough points in reference point cloud." << std::endl;
                }
                trackingProfiler.toc();
//                std::cout << "* Tracker: " << trackingProfiler << std::endl;
            } else {
                std::cout << "Failed to transfer data into tracking thread" << std::endl;
            }
        });

        trackingRunQueue.lockOnCpu(0);
        mappingRunQueue.setThreadOnCore(0, 1);
        mappingRunQueue.setThreadOnCore(1, 2);
        mappingRunQueue.setThreadOnCore(2, 3);
        mappingRunQueue.setThreadOnCore(3, 4);
    }

    boost::optional<std::pair<cv::Mat, cv::Mat>> getFramePairAtTime(int64_t t) {
    }

    void run() override {
        auto leftInput = inputs.getEventInput("left");
        auto rightInput = inputs.getEventInput("right");
        auto eventsInput = inputs.getEventInput("events");

        // Handle inputs
        if (auto in = leftInput.data()) {
            leftEvents->addEvents(dv::EventStore(in));
            std::vector<StampedMat> images = leftEvents->accumulate();
            for (const auto& image : images) {
                leftBuffer.push_back(image);
            }
        }
        if (auto in = rightInput.data()) {
        	rightEvents->addEvents(dv::EventStore(in));
        }
        if (auto ev = eventsInput.events()) {
        	events->safeAdd(dv::EventStore(ev));
        }

        if (leftBuffer.size() > 5 && mapper && tracker) {
            slam::StampedMat leftImage = leftBuffer.front();
            leftBuffer.pop_front();
            cv::Mat leftRect(leftImage.frame.size(), CV_8UC1);
            // Perform rectification only when needed
            mapper->camSysPtr_->cam_left_ptr_->rectifyImage(leftImage.frame, leftRect);

            // Run mapping thread
            if (mappingTimer.timeToRun(leftImage.timestamp)) {
                slam::StampedMat rightImage = rightEvents->accumulateWithin(leftImage.startTimestamp, leftImage.endTimestamp);
                cv::Mat rightRect(rightImage.frame.size(), CV_8UC1);
                mapper->camSysPtr_->cam_right_ptr_->rectifyImage(rightImage.frame, rightRect);
                mapper->pushFramePair(leftRect, rightRect, leftImage.timestamp);
                mappingTimestamps.push_back(leftImage.timestamp);
                if (mappingTimestamps.full()) {
                    if (initialized) {
                        mappingRunQueue.enqueue(mappingTimestamps.front());
                    } else {
                        // Initialize on a single thread to avoid multiple initializations
                        mappingRunQueue.enqueueThread(0, mappingTimestamps.front());
                    }
                }
            }

            if (initialized) {
                tracker->pushFrame(leftRect, leftImage.timestamp);
                trackingRunQueue.enqueue(leftImage.timestamp);
            }
            lastProcessTime = leftImage.endTimestamp;
        }

        // Rethrow any exceptions
        if (!mappingRunQueue.isRunning()) {
            if (auto lastException = mappingRunQueue.getLastException()) {
                trackingRunQueue.stopAutoDequeue();
                std::rethrow_exception(lastException);
            }
        }

        if (initialized && !trackingRunQueue.isRunning()) {
            if (auto lastException = trackingRunQueue.getLastException()) {
                mappingRunQueue.stopAutoDequeue();
                std::rethrow_exception(lastException);
            }
        }
    }

};

registerModuleClass(ESVOMapping)
