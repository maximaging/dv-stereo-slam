//
// Created by rokas on 2020-11-27.
//

#include <functional>

#include <dv-sdk/module.hpp>

class CropEvents : public dv::ModuleBase {
private:
    class ROI {
    public:
        int_fast16_t minX = 0;
        int_fast16_t minY = 0;
        int_fast16_t maxX = 0;
        int_fast16_t maxY = 0;
        int width = 0;
        int height = 0;
        int area = 0;

        ROI(int_fast16_t minX_, int_fast16_t minY_, int_fast16_t maxX_, int_fast16_t maxY_) : minX(minX_), minY(minY_), maxX(maxX_), maxY(maxY_) {
            width = static_cast<int>(maxX - minX);
            height = static_cast<int>(maxY - minY);
            area = width * height;
        }

        ROI() = default;

        /**
         * Performs check whether given event is within the region of interest
         * @param event     Event to be checked
         * @return          True if event belongs to the region, false otherwise
         */
        [[nodiscard]] bool contains(const dv::Event& event) const {
            return contains(event.x(), event.y());
        }

        /**
         * Performs check whether given coordinates are within the region of interest
         * @param x         Coordinate X
         * @param y         Coordinate Y
         * @return          True if coordinates belong to the region, false otherwise
         */
        [[nodiscard]] bool contains(int_fast16_t x, int_fast16_t y) const {
            return minX <= x && x < maxX && minY <= y && y < maxY;
        }

        /**
         * Checks whether coordinates are within boundaries of the roi (within region dimension limits)
         * @param x         Coordinate X
         * @param y         Coordinate Y
         * @return          True if coordinates are within dimension limits, false otherwise
         */
        [[nodiscard]] bool boundaryCheck(int_fast16_t x, int_fast16_t y) const {
            return 0 <= x && x < width && 0 <= y && y < height;
        }
    };

    ROI roi;
    bool passthrough = false;

public:
    static void initInputs(dv::InputDefinitionList &in) {
        in.addEventInput("events");
    }

    static void initOutputs(dv::OutputDefinitionList &out) {
        out.addEventOutput("events");
    }

    static void initConfigOptions(dv::RuntimeConfig &config) {
        int maxVal = std::numeric_limits<int16_t>::max();
        config.add("start_x",
                   dv::ConfigOption::intOption("Start coordinate x", 0, 0, maxVal));
        config.add("start_y",
                   dv::ConfigOption::intOption("Start coordinate y", 0, 0, maxVal));
        config.add("end_x",
                   dv::ConfigOption::intOption("End coordinate x", 0, 0, maxVal));
        config.add("end_y",
                   dv::ConfigOption::intOption("End coordinate y", 0, 0, maxVal));
    }

    static const char *initDescription() {
        return "Crop events within given ROI.";
    }

    CropEvents() {
        roi = ROI(
                static_cast<int_fast16_t>(config.getInt("start_x")),
                static_cast<int_fast16_t>(config.getInt("start_y")),
                static_cast<int_fast16_t>(config.getInt("end_x")),
                static_cast<int_fast16_t>(config.getInt("end_y"))
                );

        auto inputFrame = inputs.getEventInput("events");

        if (roi.area > 0) {
            outputs.getEventOutput("events").setup(
                    roi.width, roi.height, inputFrame.getOriginDescription()
            );
        } else {
            log.warning("Cropping ROI area is 0; The module will pass through frame unchanged.");
            outputs.getEventOutput("events").setup(
                    inputFrame.sizeX(), inputFrame.sizeY(), inputFrame.getOriginDescription()
            );
            passthrough = true;
        }
    }

    void run() override {
        auto input = inputs.getEventInput("events");
        auto output =  outputs.getEventOutput("events").events();
        if (auto inputEvents = input.data()) {
            if (passthrough) {
                outputs.getEventOutput("events") << inputEvents << dv::commit;
            } else {
                output.reserve(inputEvents.size());
                for (const dv::Event& event : inputEvents) {
                    auto new_x = event.x() - roi.minX;
                    auto new_y = event.y() - roi.minY;
                    if (roi.boundaryCheck(static_cast<int16_t>(new_x), static_cast<int16_t>(new_y))) {
                        output.emplace_back(event.timestamp(), new_x, new_y, event.polarity());
                    }
                }
                output.shrink_to_fit();
                if (!output.empty()) {
                    output.commit();
                }
            }
        }
    }

};

registerModuleClass(CropEvents)

