//
// Created by radam on 03.02.21.
//

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN  // in only one cpp file

#include "dv-slam/utilities/PoseViewer.h"

#include <boost/test/unit_test.hpp>
#include <opencv2/opencv.hpp>

const int distMax = 100;
const int angleMax = 360;
int xSlider, ySlider, zSlider;
int yawSlider, pitchSlider, rollSlider;
int frameSlider;
int mode;
int gridPlane;

cv::Mat dst;

PoseViewer viewer;

static void on_trackbar(int, void *) {
    viewer.updateCameraPosition(Eigen::Vector3f(xSlider - distMax / 2, ySlider - distMax / 2, zSlider - distMax / 2));
    viewer.updateCameraOrientation(yawSlider - angleMax / 2, pitchSlider - angleMax / 2, rollSlider - angleMax / 2);
    viewer.updateFrameSize(frameSlider + 1);
    viewer.setViewMode(static_cast<PoseViewer::Mode>(mode));
    viewer.setGridPlane(static_cast<PoseViewer::GridPlane>(gridPlane));
    dst = viewer.getImage();
    imshow("Trajectory", dst);
}

BOOST_AUTO_TEST_CASE(testPoseViewer) {
    // Test how the drawing functions of the PoseViewer work and output the images to /tmp

    const int64_t nPositions = 50;
    const int64_t maxTime = 20 * 1e6;

    viewer.setViewMode(PoseViewer::Mode::VIEW_XY);
    // Check that the pose calculation does not crash
    for (int64_t i = 0; i < nPositions; ++i) {
        // Display
        std::string path = "/tmp/" + std::to_string(i) + ".png";
        auto img = viewer.getImage();
        cv::imwrite(path, img);

        int64_t ts = maxTime / nPositions * i;

        float tss = static_cast<float>(ts) / 1e6;

        // Feed some data to the plot
        auto r = Eigen::Vector3f(sin(tss) * (tss / 5 + 1), cos(tss) * (tss / 5 + 1), tss / 8);
        auto q = Eigen::Quaternionf(1, 0, 0, 0);

        viewer.addPose(r, q, ts);
    }

    // Make it possible to play with the controls and see how the visualization behaves
    viewer.setViewMode(PoseViewer::Mode(mode));
    xSlider = distMax / 2 + 0;
    ySlider = distMax / 2 + 0;
    zSlider = distMax / 2 + -5;
    yawSlider = angleMax / 2 + 0;
    pitchSlider = angleMax / 2 + 0;
    rollSlider = angleMax / 2 + 0;
    frameSlider = 1;
    mode = 0;
    namedWindow("Trajectory", cv::WINDOW_AUTOSIZE); // Create Window
    char xName[50];
    sprintf(xName, "x - %d", distMax / 2);
    char yName[50];
    sprintf(yName, "y - %d", distMax / 2);
    char zName[50];
    sprintf(zName, "z  - %d", distMax / 2);
    char yawName[50];
    sprintf(yawName, "yaw [deg] - %d", angleMax / 2);
    char pitchName[50];
    sprintf(pitchName, "pitch [deg] - %d", angleMax / 2);
    char rollName[50];
    sprintf(rollName, "roll [deg] - %d", angleMax / 2);
    char frameName[50];
    sprintf(frameName, "Frame size [m] %d", distMax);
    char modeName[50];
    sprintf(modeName, "Mode [0-6] %d", 6);
    char gridPlaneName[50];
    sprintf(gridPlaneName, "Grid plane [0-3] %d", 3);
    cv::createTrackbar(xName, "Trajectory", &xSlider, distMax, on_trackbar);
    cv::createTrackbar(yName, "Trajectory", &ySlider, distMax, on_trackbar);
    cv::createTrackbar(zName, "Trajectory", &zSlider, distMax, on_trackbar);
    cv::createTrackbar(yawName, "Trajectory", &yawSlider, angleMax, on_trackbar);
    cv::createTrackbar(pitchName, "Trajectory", &pitchSlider, angleMax, on_trackbar);
    cv::createTrackbar(rollName, "Trajectory", &rollSlider, angleMax, on_trackbar);
    cv::createTrackbar(frameName, "Trajectory", &frameSlider, distMax, on_trackbar);
    cv::createTrackbar(modeName, "Trajectory", &mode, 6, on_trackbar);
    cv::createTrackbar(gridPlaneName, "Trajectory", &gridPlane, 3, on_trackbar);
    on_trackbar(xSlider, 0);
    on_trackbar(ySlider, 0);
    on_trackbar(zSlider, 0);
    on_trackbar(yawSlider, 0);
    on_trackbar(pitchSlider, 0);
    on_trackbar(rollSlider, 0);
    cv::waitKey(0);


    // Just a smoke test, nothing to check
    BOOST_CHECK(true);

}
