#include <esvo_core/core/RegProblemLM.h>
#include <esvo_core/tools/cayley.h>
#include <thread>
#include <algorithm>

namespace esvo_core
{
    namespace core
    {
        RegProblemLM::RegProblemLM(
                const CameraSystem::Ptr& camSysPtr,
                const RegProblemConfig::Ptr& rpConfig_ptr,
                size_t numThread):
                optimization::OptimizationFunctor<float>(6,0),
                camSysPtr_(camSysPtr),
                rpConfigPtr_(rpConfig_ptr),
                NUM_THREAD_(numThread),
                bPrint_(false)
        {
            patchSize_ = rpConfigPtr_->patchSize_X_ * rpConfigPtr_->patchSize_Y_;
            computeJ_G(Eigen::Matrix<float,6,1>::Zero(), J_G_0_);
        }

        void RegProblemLM::setProblem(RefFrame* ref, CurFrame* cur, bool bComputeGrad)
        {
            ref_ = ref;
            cur_ = cur;
            T_world_ref_  = ref_->tr_.getTransformationMatrix().cast<float>();
            T_world_left_ = cur_->tr_.getTransformationMatrix().cast<float>();
            Eigen::Matrix4f T_ref_left = T_world_ref_.inverse() * T_world_left_;
            R_ = T_ref_left.block<3,3>(0,0);
            t_ = T_ref_left.block<3,1>(0,3);
            Eigen::Matrix3f R_world_ref = T_world_ref_.block<3,3>(0,0);
            Eigen::Vector3f t_world_ref = T_world_ref_.block<3,1>(0,3);

            // load ref's pointcloud tp vResItem
            ResItems_.clear();
            numPoints_ =ref_->vPointXYZPtr_.size();
            if(numPoints_ > rpConfigPtr_->MAX_REGISTRATION_POINTS_)
                numPoints_ = rpConfigPtr_->MAX_REGISTRATION_POINTS_;
            ResItems_.resize(numPoints_);
            if(bPrint_)
                LOG(INFO) << "num points: " << numPoints_;
            bool bStochasticSampling = true;
            auto R_world_ref_t = R_world_ref.transpose();
            // TODO: Grid based sampling instead of stochastic sampling?
            auto residualIter = ResItems_.begin();
            for(size_t i = 0; i < ref->vPointXYZPtr_.size(); i++) {
                if (bStochasticSampling)
                    std::swap(ref->vPointXYZPtr_[i], ref->vPointXYZPtr_[i + rand() % (ref->vPointXYZPtr_.size() - i)]);
                auto& point = ref->vPointXYZPtr_[i];
                Eigen::Vector3f p_tmp(point.x, point.y, point.z);
                Eigen::Vector3f p_cam = R_world_ref_t * (p_tmp - t_world_ref);
                Eigen::Vector2f pixelPoint;
                camSysPtr_->cam_left_ptr_->world2Cam(p_cam, pixelPoint);
                // Select points that are only within the undistorted image region, skip outlier points
                if (camSysPtr_->cam_left_ptr_->testUndistortionMask(cv::Point2i(pixelPoint(0), pixelPoint(1)))) {
                    residualIter->initialize(p_cam);
                    residualIter++;
                    // Until all residual items are initialized
                    if (residualIter == ResItems_.end()) {
                        break;
                    }
                }

            }
            // for stochastic sampling
            numBatches_ = std::max(ResItems_.size() / rpConfigPtr_->BATCH_SIZE_, (size_t)1);

            // load cur's info
            pTsObs_ = cur->pTsObs_;
            // set fval dimension
            resetNumberValues(numPoints_ * patchSize_);
            if(bPrint_)
                LOG(INFO) << "RegProblemLM::setProblem succeeds.";
        }

        void RegProblemLM::setStochasticSampling(size_t offset, size_t N)
        {
            ResItemsStochSampled_.clear();
            ResItemsStochSampled_.reserve(N);
            size_t limit = ResItems_.size();
            for(size_t i = 0;i < N;i++)
            {
                if(offset + i >= limit)
                    break;
                ResItemsStochSampled_.push_back(ResItems_[offset + i]);
            }
            numPoints_ = ResItemsStochSampled_.size();
            resetNumberValues(numPoints_ * patchSize_);
            if(bPrint_)
            {
                LOG(INFO) << "offset: " << offset;
                LOG(INFO) << "N: " << N;
                LOG(INFO) << "ResItems_.size: " << ResItems_.size();
                LOG(INFO) << "ResItemsStochSampled_.size: " << ResItemsStochSampled_.size();
            }
        }

        int RegProblemLM::operator()(const Eigen::Matrix<float,6,1>& x, Eigen::VectorXf& fvec) const
        {
            // calculate the warping transformation (T_cur_ref))
            Eigen::Matrix4f T_warping = Eigen::Matrix4f::Identity();
            getWarpingTransformation(T_warping, x);

            if (NUM_THREAD_ == 1) {
                Job job = Job();
                job.pvRI_ = const_cast<ResidualItems*>(&ResItemsStochSampled_);
                job.pTsObs_ = const_cast<TimeSurfaceObservation*>(pTsObs_);
                job.T_left_ref_ = const_cast<Eigen::Matrix4f*>(&T_warping);
                job.i_thread_ = 0;

                // No multithreading - more predictable execution in single thread mode.
                RegProblemLM::thread(job);
            } else {
                // warp and calculate the residual
                std::vector<Job> jobs(NUM_THREAD_);
                for(size_t i = 0;i < NUM_THREAD_;i++)
                {
                    jobs[i].pvRI_ = const_cast<ResidualItems*>(&ResItemsStochSampled_);
                    jobs[i].pTsObs_ = const_cast<TimeSurfaceObservation*>(pTsObs_);
                    jobs[i].T_left_ref_ = const_cast<Eigen::Matrix4f*>(&T_warping);
                    jobs[i].i_thread_ = i;
                }

                std::vector<std::thread> threads;
                for(size_t i = 0; i < NUM_THREAD_; i++) {
                    threads.emplace_back(std::bind(&RegProblemLM::thread, this, jobs[i]));
                }
                for( auto& thread : threads) {
                    if(thread.joinable()){
                        thread.join();
                    }
                }

            }

            size_t resItemCount = ResItemsStochSampled_.size();
            if(rpConfigPtr_->LSnorm_ == RegProblemConfig::Huber) {
                long i = 0;
                for (const auto& ri : ResItemsStochSampled_) {
                    float residual = ri.residual_(0);
                    if (residual > rpConfigPtr_->huber_threshold_) {
                        fvec[i] = sqrt(rpConfigPtr_->huber_threshold_ / residual) * residual;
                    } else {
                        fvec[i] = residual;
                    }
                    i++;
                }
            } else if(rpConfigPtr_->LSnorm_ == RegProblemConfig::l2) {
                // assign the reweighted residual to fvec
                for(size_t i = 0; i < resItemCount; i++)
                {
                    ResidualItem & ri = const_cast<ResidualItem&>(ResItemsStochSampled_[i]);
                    fvec.segment(i * ri.residual_.size(), ri.residual_.size()) = ri.residual_;// / sqrt(var);
                }
            }

//  LOG(INFO) << "assign weighted residual ..............";
            return 0;
        }

        void
        RegProblemLM::thread(Job& job ) const
        {
            // load info from job
            ResidualItems & vRI = *job.pvRI_;
            const TimeSurfaceObservation & TsObs = *job.pTsObs_;
            const Eigen::Matrix4f & T_left_ref = *job.T_left_ref_;
            auto warpingR = T_left_ref.block<3, 3>(0, 0);
            auto warpingT = T_left_ref.block<3, 1>(0, 3);
            size_t i_thread = job.i_thread_;
            size_t numPoint = vRI.size();
            size_t wx = rpConfigPtr_->patchSize_X_;
            size_t wy = rpConfigPtr_->patchSize_Y_;
            size_t residualDim = wx * wy;

            // calculate point-wise spatio-temporal residual
            // the residual can be either a scalr or a vector, up to the residualDim.
            for(size_t i = i_thread; i < numPoint; i+= NUM_THREAD_)
            {
                ResidualItem & ri = vRI[i];
                ri.residual_ = Eigen::VectorXf(residualDim);
                Eigen::Vector2f x1_s;
                if(!reprojection(warpingR * ri.p_ + warpingT, x1_s))
                    ri.residual_.setConstant(255.0);
                else
                {
                    if (residualDim == 1) {
                        ri.residual_[0] = patchInterpolation1x1(TsObs.cv_sae_flipped_left, x1_s);
                    } else {
                        Eigen::MatrixXf tau1;
                        if(patchInterpolation(TsObs.cv_sae_flipped_left, x1_s, tau1))
                        {
                            for(size_t y = 0; y < wy; y++)
                                for(size_t x = 0; x < wx; x++)
                                {
                                    size_t index = y * wx + x;
                                    ri.residual_[index] = tau1(y,x);
                                }
                        }
                        else
                            ri.residual_.setConstant(255.0);
                    }
                }
            }
        }

        int RegProblemLM::df(const Eigen::Matrix<float,6,1>& x, Eigen::MatrixXf& fjac) const
        {
            if(x != Eigen::Matrix<float,6,1>::Zero())
            {
                LOG(INFO) << "The Jacobian is not evaluated at Zero !!!!!!!!!!!!!";
                exit(-1);
            }
            fjac.resize(m_values, 6);

            // J_x = dPi_dT * dT_dInvPi * dInvPi_dx
            Eigen::Matrix3f dT_dInvPi = R_.transpose();// Explaination for the transpose() can be found below.
            Eigen::Matrix<float,3,2> dInvPi_dx_constPart;
            dInvPi_dx_constPart.setZero();
            dInvPi_dx_constPart(0,0) = 1.0f / camSysPtr_->cam_left_ptr_->R_f(0,0);
            dInvPi_dx_constPart(1,1) = 1.0f / camSysPtr_->cam_left_ptr_->R_f(1,1);
            Eigen::Matrix<float,3,2> J_constPart = dT_dInvPi * dInvPi_dx_constPart;

            // J_theta = dPi_dT * dT_dG * dG_dtheta
            // Assemble the Jacobian without dG_dtheta.
            Eigen::MatrixXf fjacBlock;
            fjacBlock.resize(numPoints_, 12);
//            Eigen::MatrixXf fjacTMP(3,6);//FOR Test
//            Eigen::Matrix4f T_left_ref = Eigen::Matrix4f::Identity();
//            T_left_ref.block<3,3>(0,0) = dT_dInvPi;
//            T_left_ref.block<3,1>(0,3) = -dT_dInvPi * t_;
            auto warpingT = -dT_dInvPi * t_;

            const float P11 = static_cast<const float>(camSysPtr_->cam_left_ptr_->P_(0, 0));
            const float P12 = static_cast<const float>(camSysPtr_->cam_left_ptr_->P_(0, 1));
            const float P14 = static_cast<const float>(camSysPtr_->cam_left_ptr_->P_(0, 3));
            const float P21 = static_cast<const float>(camSysPtr_->cam_left_ptr_->P_(1, 0));
            const float P22 = static_cast<const float>(camSysPtr_->cam_left_ptr_->P_(1, 1));
            const float P24 = static_cast<const float>(camSysPtr_->cam_left_ptr_->P_(1, 3));
            // assemble dT_dG
            Eigen::Matrix<float,3,12> dT_dG;
            dT_dG.setZero();
            Eigen::Matrix<float,2,3> dPi_dT;
            dPi_dT.setZero();
            dT_dG.block<3,3>(0,9) = Eigen::Matrix3f::Identity();
            for(long i = 0; i < numPoints_; i++)
            {
                Eigen::Vector2f x1_s;
                const ResidualItem & ri = ResItemsStochSampled_[i];
                if(!reprojection(dT_dInvPi * ri.p_ + warpingT, x1_s))
                    fjacBlock.row(i) = Eigen::Matrix<float,1,12>::Zero();
                else
                {
                    // obtain the exact gradient by bilinear interpolation.
                    float gx, gy;
//                    Eigen::MatrixXd negative_du, negative_dv;
                    if (rpConfigPtr_->patchSize_X_ == 1 && rpConfigPtr_->patchSize_Y_ == 1) {
                        patchInterpolation1x1Sobel(pTsObs_->cv_dFlippedSAE_du_left, pTsObs_->cv_dFlippedSAE_dv_left, x1_s, gx, gy);
                    } else {
                        Eigen::MatrixXf gxMat, gyMat;
                        patchInterpolation(pTsObs_->cv_dFlippedSAE_du_left, x1_s, gxMat);
                        patchInterpolation(pTsObs_->cv_dFlippedSAE_dv_left, x1_s, gyMat);
                        gx = gxMat(0, 0);
                        gy = gyMat(0, 0);
                    }
                    Eigen::Vector2f grad = Eigen::Vector2f(gx / 8.f, gy / 8.f);
                    float ri_x = ri.p_(0);
                    float ri_y = ri.p_(1);
                    float ri_z = ri.p_(2);

                    dPi_dT.block<2,2>(0,0) = camSysPtr_->cam_left_ptr_->P_.block<2,2>(0,0).cast<float>() / ri_z;
                    const float z2 = ri_z * ri_z;
                    dPi_dT(0,2) = -(P11 * ri_x + P12 * ri_y + P14) / z2;
                    dPi_dT(1,2) = -(P21 * ri_x + P22 * ri_y + P24) / z2;

                    dT_dG(0, 0) = ri_x;
                    dT_dG(1, 1) = ri_x;
                    dT_dG(2, 2) = ri_x;
                    dT_dG(0, 3) = ri_y;
                    dT_dG(1, 4) = ri_y;
                    dT_dG(2, 5) = ri_y;
                    dT_dG(0, 6) = ri_z;
                    dT_dG(1, 7) = ri_z;
                    dT_dG(2, 8) = ri_z;
//      LOG(INFO) << "dT_dG:\n" << dT_dG;
                    fjacBlock.row(i).noalias() = grad.transpose() * dPi_dT * J_constPart * dPi_dT * dT_dG * ri_z;//ri.p_(2) refers to 1/rho_i which is actually coming with dInvPi_dx.
                }
            }
            // assemble with dG_dtheta
            fjac = -fjacBlock * J_G_0_;
            // The explanation for the factor -1 is as follows. The transformation recovered from dThetha
            // is T_right_left (R_, t_). However, the one used for warping is T_left_right (R_.transpose(), -R.transpose() * t).
            // Thus, R_.transpose() is used as dT_dInvPi. Besides, J_theta = dPi_dT * dT_dG' * dG'_dG * dG_dtheta. G'(dtheta) recovers
            // the motion for the warping, namely R_.transpose(), -R.transpose() * t.
            //          /                                 \
  //          | 1 0 0 0 0 0 0 0 0       | 0 0 0 |
            //          | 0 0 0 1 0 0 0 0 0       | 0 0 0 |
            //          | 0 0 0 0 0 0 1 0 0       | 0 0 0 |
            //          | 0 1 0 0 0 0 0 0 0       | 0 0 0 |
            //          | 0 0 0 0 1 0 0 0 0       | 0 0 0 |
            // dG'_dG = | 0 0 0 0 0 0 0 1 0       | 0 0 0 |
            //          | 0 0 1 0 0 0 0 0 0       | 0 0 0 |
            //          | 0 0 0 0 0 1 0 0 0       | 0 0 0 |
            //          | 0 0 0 0 0 0 0 0 1       | 0 0 0 |
            //          | -tx -ty -tz 0 0 0 0 0 0 | -r_{11} -r_{21} -r_{31}|
            //          | 0 0 0 -tx -ty -tz 0 0 0 | -r_{12} -r_{22} -r_{32}|
            //          | 0 0 0 0 0 0 -tx -ty -tz | -r_{13} -r_{23} -r_{33}|
            //          \                                                  / 12 x 12
            // The linearization is performed around dtheta = 0, thus tx = ty = tz = 0, r_{ii} = 1, r_{ij} = 0.
            // dG'_dG * dG_dtheta = -dG_dtheta. This explains where is "-1" from.

            // LOG(INFO) << "fjac:\n" << fjac;
            // LOG(INFO) << "Jacobian Computation takes " << tt.toc() << " ms.";
            return 0;
        }

        void
        RegProblemLM::computeJ_G(const Eigen::Matrix<float,6,1>&x, Eigen::Matrix<float,12,6>& J_G)
        {
            assert( x.size() == 6 );
            assert( J_G.rows() == 12 && J_G.cols() == 6 );
            float c1, c2, c3, k;
            float c1_sq, c2_sq, c3_sq, k_sq;
            c1 = x(0);
            c2 = x(1);
            c3 = x(2);
            c1_sq = c1 * c1;
            c2_sq = c2 * c2;
            c3_sq = c3 * c3;
            k = 1 + c1_sq + c2_sq + c3_sq;
            k_sq = k * k;
            Eigen::Matrix3f A1, A2, A3;
            // A1
            A1(0,0) = 2*c1 / k  - 2*c1*(1 + c1_sq - c2_sq - c3_sq) / k_sq;
            A1(0,1) = -2*c2 / k - 2*c2*(1 + c1_sq - c2_sq - c3_sq) / k_sq;
            A1(0,2) = -2*c3 / k - 2*c3*(1 + c1_sq - c2_sq - c3_sq) / k_sq;
            A1(1,0) = 2*c2 / k - 4*c1*(c1*c2 + c3) / k_sq;
            A1(1,1) = 2*c1 / k - 4*c2*(c1*c2 + c3) / k_sq;
            A1(1,2) = 2 / k    - 4*c3*(c1*c2 + c3) / k_sq;
            A1(2,0) = 2*c3 / k - 4*c1*(c1*c3 - c2) / k_sq;
            A1(2,1) = -2 / k   + 4*c2*(c1*c3 - c2) / k_sq;
            A1(2,2) = 2*c1 / k - 4*c3*(c1*c3 - c2) / k_sq;
            //A2
            A2(0,0) = 2*c2 / k - 4*c1*(c1*c2 - c3) / k_sq;
            A2(0,1) = 2*c1 / k - 4*c2*(c1*c2 - c3) / k_sq;
            A2(0,2) = -2 / k   - 4*c3*(c1*c2 - c3) / k_sq;
            A2(1,0) = -2*c1 / k - 2*c1*(1 - c1_sq + c2_sq - c3_sq) / k_sq;
            A2(1,1) = 2*c2 / k  - 2*c2*(1 - c1_sq + c2_sq - c3_sq) / k_sq;
            A2(1,2) = -2*c3 / k - 2*c3*(1 - c1_sq + c2_sq - c3_sq) / k_sq;
            A2(2,0) = 2 / k    - 4*c1*(c1 + c2*c3) / k_sq;
            A2(2,1) = 2*c3 / k - 4*c2*(c1 + c2*c3) / k_sq;
            A2(2,2) = 2*c2 / k - 4*c3*(c1 + c2*c3) / k_sq;
            //A3
            A3(0,0) = 2*c3 / k - 4*c1*(c2 + c1*c3) / k_sq;
            A3(0,1) = 2 / k    - 4*c2*(c2 + c1*c3) / k_sq;
            A3(0,2) = 2*c1 / k - 4*c3*(c2 + c1*c3) / k_sq;
            A3(1,0) = -2 / k   - 4*c1*(c2*c3 - c1) / k_sq;
            A3(1,1) = 2*c3 / k - 4*c2*(c2*c3 - c1) / k_sq;
            A3(1,2) = 2*c2 / k - 4*c3*(c2*c3 - c1) / k_sq;
            A3(2,0) = -2*c1 / k - 2*c1*(1 - c1_sq - c2_sq + c3_sq) / k_sq;
            A3(2,1) = -2*c2 / k - 2*c2*(1 - c1_sq - c2_sq + c3_sq) / k_sq;
            A3(2,2) = 2*c3 / k  - 2*c3*(1 - c1_sq - c2_sq + c3_sq) / k_sq;

            Eigen::Matrix3f O33 = Eigen::MatrixXf::Zero(3,3);
            Eigen::Matrix3f I33 = Eigen::MatrixXf::Identity(3,3);
            J_G.block<3,3>(0,0) = A1;  J_G.block<3,3>(0,3) = O33;
            J_G.block<3,3>(3,0) = A2;  J_G.block<3,3>(3,3) = O33;
            J_G.block<3,3>(6,0) = A3;  J_G.block<3,3>(6,3) = O33;
            J_G.block<3,3>(9,0) = O33; J_G.block<3,3>(9,3) = I33;
        }

        void
        RegProblemLM::getWarpingTransformation(
                Eigen::Matrix4f& warpingTransf,
                const Eigen::Matrix<float, 6, 1>& x) const
        {
            // get delta cayley paramters (this corresponds to the delta motion of the ref frame)
            Eigen::Vector3f dc = x.block<3,1>(0,0);
            Eigen::Vector3f dt = x.block<3,1>(3,0);
            // add rotation
            Eigen::Matrix3f dR = tools::cayley2rot(dc);
            Eigen::Matrix3f newR = R_.transpose() * dR.transpose();
            Eigen::JacobiSVD<Eigen::Matrix3f> svd(newR, Eigen::ComputeFullU | Eigen::ComputeFullV );
            Eigen::Matrix3f R_cur_ref = svd.matrixU() * svd.matrixV().transpose();
            if( R_cur_ref.determinant() < 0.0f )
            {
                LOG(INFO) << "oops the matrix is left-handed\n";
                exit(-1);
            }
            warpingTransf.block<3,3>(0,0) = R_cur_ref;
            warpingTransf.block<3,1>(0,3) = -R_cur_ref * ( dt + dR * t_ );
        }

        void
        RegProblemLM::addMotionUpdate(const Eigen::Matrix<float, 6, 1>& dx)
        {
            // To update R_, t_
            Eigen::Vector3f dc = dx.block<3,1>(0,0);
            Eigen::Vector3f dt = dx.block<3,1>(3,0);
            // add rotation
            Eigen::Matrix3f dR = tools::cayley2rot(dc);
            Eigen::Matrix3f newR = dR * R_;
            Eigen::JacobiSVD<Eigen::Matrix3f> svd(newR, Eigen::ComputeFullU | Eigen::ComputeFullV );
            R_.noalias() = svd.matrixU() * svd.matrixV().transpose();
            t_ = dt + dR * t_;
        }

        void RegProblemLM::setPose()
        {
            T_world_left_.block<3,3>(0,0) = T_world_ref_.block<3,3>(0,0) * R_;
            T_world_left_.block<3,1>(0,3) = T_world_ref_.block<3,3>(0,0) * t_
                                            + T_world_ref_.block<3,1>(0,3);
            Eigen::Matrix<double, 4, 4> f = T_world_left_.cast<double>();
            cur_->tr_ = Transformation(
                    kindr::minimal::RotationQuaternion::fromApproximateRotationMatrix(f.block<3,3>(0,0)),
                    f.block<3, 1>(0, 3)
                    );
//  LOG(INFO) << "T_world_ref_\n " << T_world_ref_ << "\n ";
//  LOG(INFO) << "T_world_left_\n " << T_world_left_ << "\n ";
//  LOG(INFO) << "R_\n " << R_ << "\n ";
//  LOG(INFO) << "t_\n " << t_.transpose() << "\n ";
        }

        Eigen::Matrix4d
        RegProblemLM::getPose()
        {
            return T_world_left_.cast<double>();
        }

        bool RegProblemLM::isValidPatch(
                Eigen::Vector2f& patchCentreCoord,
                Eigen::MatrixXi& mask,
                size_t wx,
                size_t wy) const
        {
            if (patchCentreCoord(0) < (wx-1)/2 ||
                patchCentreCoord(0) > camSysPtr_->cam_left_ptr_->width_  - (wx-1)/2 - 1||
                patchCentreCoord(1) < (wy-1)/2 ||
                patchCentreCoord(1) > camSysPtr_->cam_left_ptr_->height_ - (wy-1)/2 - 1)
                return false;
            if(mask(static_cast<size_t>(patchCentreCoord(1)-(wy-1)/2), static_cast<size_t>(patchCentreCoord(0)-(wx-1)/2)) < 125)
                return false;
            if(mask(static_cast<size_t>(patchCentreCoord(1)-(wy-1)/2), static_cast<size_t>(patchCentreCoord(0)+(wx-1)/2)) < 125)
                return false;
            if(mask(static_cast<size_t>(patchCentreCoord(1)+(wy-1)/2), static_cast<size_t>(patchCentreCoord(0)-(wx-1)/2)) < 125)
                return false;
            if(mask(static_cast<size_t>(patchCentreCoord(1)+(wy-1)/2), static_cast<size_t>(patchCentreCoord(0)+(wx-1)/2)) < 125)
                return false;
            return true;
        }

        bool RegProblemLM::isValidPatch1x1(Eigen::Vector2f& patchCentreCoord, Eigen::MatrixXi& mask) const {
            return !(patchCentreCoord(0) <= 0.f ||
                    patchCentreCoord(0) > static_cast<float>(camSysPtr_->cam_left_ptr_->width_ - 2) ||
                    patchCentreCoord(1) <= 0.f ||
                    patchCentreCoord(1) > static_cast<float>(camSysPtr_->cam_left_ptr_->height_ - 2)) &&
                    mask(static_cast<size_t>(patchCentreCoord(1)), static_cast<size_t>(patchCentreCoord(0))) >= 125;
        }

        bool RegProblemLM::reprojection(
                const Eigen::Vector3f& p,
                const Eigen::Matrix4f& warpingTransf,
                Eigen::Vector2f &x1_s) const
        {
            // transfer to left DVS coordinate
            Eigen::Vector3f p_left =
                    warpingTransf.block<3, 3>(0, 0) * p + warpingTransf.block<3, 1>(0, 3);
            camSysPtr_->cam_left_ptr_->world2Cam(p_left, x1_s);
            if (rpConfigPtr_->patchSize_X_ == 1 && rpConfigPtr_->patchSize_Y_ == 1) {
                return isValidPatch1x1(x1_s, camSysPtr_->cam_left_ptr_->UndistortRectify_mask_);
            } else {
                return isValidPatch(x1_s, camSysPtr_->cam_left_ptr_->UndistortRectify_mask_,
                                    rpConfigPtr_->patchSize_X_, rpConfigPtr_->patchSize_Y_);
            }
        }

        bool RegProblemLM::reprojection(
                const Eigen::Vector3f& p_left,
                Eigen::Vector2f &x1_s) const
        {
            // transfer to left DVS coordinate
            camSysPtr_->cam_left_ptr_->world2Cam(p_left, x1_s);
            if (rpConfigPtr_->patchSize_X_ == 1 && rpConfigPtr_->patchSize_Y_ == 1) {
                return isValidPatch1x1(x1_s, camSysPtr_->cam_left_ptr_->UndistortRectify_mask_);
            } else {
                return isValidPatch(x1_s, camSysPtr_->cam_left_ptr_->UndistortRectify_mask_,
                                    rpConfigPtr_->patchSize_X_, rpConfigPtr_->patchSize_Y_);
            }
        }

bool RegProblemLM::patchInterpolation(
  const cv::Mat &img,
  const Eigen::Vector2f &location,
  Eigen::MatrixXf &patch,
  bool debug) const
{
  int wx = rpConfigPtr_->patchSize_X_;
  int wy = rpConfigPtr_->patchSize_Y_;
  // compute SrcPatch_UpLeft coordinate and SrcPatch_DownRight coordinate
  // check patch bourndary is inside img boundary
  Eigen::Vector2i SrcPatch_UpLeft, SrcPatch_DownRight;
  SrcPatch_UpLeft << floor(location[0]) - (wx - 1) / 2, floor(location[1]) - (wy - 1) / 2;
  SrcPatch_DownRight << floor(location[0]) + (wx - 1) / 2, floor(location[1]) + (wy - 1) / 2;

  if (SrcPatch_UpLeft[0] < 0 || SrcPatch_UpLeft[1] < 0)
  {
    if(debug)
    {
      LOG(INFO) << "patchInterpolation 1: " << SrcPatch_UpLeft.transpose();
    }
    return false;
  }
  if (SrcPatch_DownRight[0] >= img.cols || SrcPatch_DownRight[1] >= img.rows)
  {
    if(debug)
    {
      LOG(INFO) << "patchInterpolation 2: " << SrcPatch_DownRight.transpose();
    }
    return false;
  }

  // compute q1 q2 q3 q4
  Eigen::Vector2f double_indices;
  double_indices << location[1], location[0];

  std::pair<int, int> lower_indices(floor(double_indices[0]), floor(double_indices[1]));
  std::pair<int, int> upper_indices(lower_indices.first + 1, lower_indices.second + 1);

    float q1 = upper_indices.second - double_indices[1];
    float q2 = double_indices[1] - lower_indices.second;
    float q3 = upper_indices.first - double_indices[0];
    float q4 = double_indices[0] - lower_indices.first;

  // extract Src patch, size (wy+1) * (wx+1)
  int wx2 = wx + 1;
  int wy2 = wy + 1;
  if (SrcPatch_UpLeft[1] + wy >= img.rows || SrcPatch_UpLeft[0] + wx >= img.cols)
  {
    if(debug)
    {
      LOG(INFO) << "patchInterpolation 3: " << SrcPatch_UpLeft.transpose()
                << ", location: " << location.transpose()
                << ", floor(location[0]): " << floor(location[0])
                << ", (wx - 1) / 2: " << (wx - 1) / 2
                << ", ans: " << floor(location[0]) - (wx - 1) / 2
                << ", wx: " << wx << " wy: " << wy
                << ", img.row: " << img.rows << " img.col: " << img.cols;
    }
    return false;
  }
  Eigen::MatrixXf SrcPatch;
  cv::Mat croppedPatch = img(cv::Rect(SrcPatch_UpLeft[0], SrcPatch_UpLeft[1], wx2, wy2));
  cv::cv2eigen(croppedPatch, SrcPatch);

  // Compute R, size (wy+1) * wx.
  Eigen::MatrixXf R;
  R.noalias() = q1 * SrcPatch.block(0, 0, wy2, wx) + q2 * SrcPatch.block(0, 1, wy2, wx);

  // Compute F, size wy * wx.
  patch.noalias() = q3 * R.block(0, 0, wy, wx) + q4 * R.block(1, 0, wy, wx);
  return true;
}

    float RegProblemLM::patchInterpolation1x1(
            const cv::Mat &img,
            const Eigen::Vector2f &location) {
        int colStart = static_cast<int>(std::floor(location(0)));
        int rowStart = static_cast<int>(std::floor(location(1)));
        cv::Range colRange(colStart, colStart + 1);
        cv::Range rowRange(rowStart, rowStart + 1);
        float patch = 0.f;
        for (int r = rowRange.start; r < rowRange.end; r++) {
            for (int c = colRange.start; c < colRange.end; c++) {
                patch += static_cast<float>(img.at<uint8_t>(r, c));
            }
        }
        return patch;
    }

    bool RegProblemLM::patchInterpolation1x1Sobel(
            const cv::Mat &sobelX,
            const cv::Mat &sobelY,
            const Eigen::Vector2f &location,
            float &gx,
            float &gy
            ) {
        int colStart = static_cast<int>(std::floor(location(0)));
        int rowStart = static_cast<int>(std::floor(location(1)));
        cv::Range colRange(colStart, colStart + 1);
        cv::Range rowRange(rowStart, rowStart + 1);
//        if (colStart >= 0 && rowStart >= 0 && colRange.end < sobelX.cols && rowRange.end < sobelX.rows) {
            gx = 0.f;
            gy = 0.f;
            for (int r = rowRange.start; r < rowRange.end; r++) {
                for (int c = colRange.start; c < colRange.end; c++) {
                    gx += static_cast<float>(sobelX.at<int16_t>(r, c));
                    gy += static_cast<float>(sobelY.at<int16_t>(r, c));
                }
            }
            return true;
//        } else {
//            return false;
//        }
    }


    }
}
