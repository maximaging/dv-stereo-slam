#include <esvo_core/container/ResidualItem.h>

esvo_core::container::ResidualItem::ResidualItem() = default;

esvo_core::container::ResidualItem::ResidualItem(
  const float x,
  const float y,
  const float z)
{
  initialize(x,y,z);
}

void esvo_core::container::ResidualItem::initialize(
  const float x,
  const float y,
  const float z)
{
  p_.noalias() = Eigen::Vector3f(x,y,z);
//  bOutlier_ = false;
//  variance_ = 1.0;
}

void esvo_core::container::ResidualItem::initialize(const Eigen::Vector3f& v) {
    p_.noalias() = v;
}
