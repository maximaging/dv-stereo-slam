#pragma once

#include <esvo_core/container/CameraSystem.h>
#include <esvo_core/core/RegProblemLM.h>
#include <esvo_core/core/RegProblemSolverLM.h>
#include <esvo_core/tools/utils.h>
#include <esvo_core/tools/Visualization.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <map>
#include <deque>
#include <mutex>
#include <future>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <dv-slam/utilities/Transformer.h>

using namespace esvo_core;
using namespace esvo_core::core;


enum TrackingStatus
{
    IDLE,
    WORKING
};

class ESVOStandAloneTracker
{
    enum SystemStatus
    {
        INITIALIZATION,
        SYSTEM_WORKING
    };

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    explicit ESVOStandAloneTracker(std::string  calibration);

    // functions regarding tracking
    void refDataTransferring();
    bool curDataTransferring(int64_t timestamp);// These two data transferring functions are decoupled because the data are not updated at the same frequency.
    bool track();

    // topic callback functions
    void pushEvents(const std::shared_ptr<const dv::EventPacket>& eventsPtr);
    void pushFrame(cv::Mat &left, int64_t t_new_TS);
    void pushReferencePointCloud(const PointCloud& pointCloud, int64_t t_new_TS);


    std::pair<int64_t, Transformation> getWorldTransform() const;

    cv::Mat visualizeTracking();
    Transformer tf_;

    void setMinInvDepthRange(double minInvDepth);
    void setMaxInvDepthRange(double maxInvDepth);
    void setContrast(double contrast);

private:
    // offline data
    std::string dvs_frame_id_;
    std::string world_frame_id_;
    std::string calibInfoDir_;
    CameraSystem::Ptr camSysPtr_;

    // online data
    EventQueue events_left_;
    TimeSurfaceHistory TS_history_;
    size_t TS_id_;
    RefPointCloudMap refPCMap_;
    RefFrame ref_;
    CurFrame cur_;

    std::mutex data_mutex_;

    /**** offline parameters ***/
    size_t TS_HISTORY_LENGTH_;
    size_t REF_HISTORY_LENGTH_;
    bool bSaveTrajectory_;
    bool bVisualizeTrajectory_;
    std::string resultPath_;

    Eigen::Matrix<double, 4, 4> T_world_ref_;
    Eigen::Matrix<double, 4, 4> T_world_cur_;

    /*** system objects ***/
    RegProblemType rpType_;
    TrackingStatus ets_;
    SystemStatus ESVO_System_Status_;
    RegProblemConfig::Ptr rpConfigPtr_;
    RegProblemSolverLM rpSolver_;


};
