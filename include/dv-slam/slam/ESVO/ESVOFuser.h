//
// Created by rokas on 2020-12-23.
//

#pragma once

#include <esvo_core/core/DepthFusion.h>
#include <esvo_core/core/DepthProblem.h>
#include <esvo_core/core/DepthRegularization.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include "SparseDepthFrame.h"

namespace slam {
    enum FusionStrategy {
        ConstPoints, ConstFrames
    };

    class ESVOFuser {
    protected:
        std::shared_ptr<esvo_core::core::DepthFusion> dFusor_;

        bool initiatilized = false;

        FusionStrategy fusionStrategy = FusionStrategy::ConstPoints;

        size_t frameCount = 20;

        size_t pointCount = 2000;

        std::map<int64_t, DepthPointsPtr> frameHistory;

        bool regularization = true;

        esvo_core::core::DepthRegularization::Ptr dRegularizor_;

        size_t frameCounter = 0;
        size_t frameWidth = 0;
        size_t frameHeight = 0;

        int64_t lastFusionTimestamp = 0LL;

        double age_vis_threshold_ = 1.0;
        double invDepth_max_range_;
        double invDepth_min_range_;
        double stdVar_vis_threshold_;
        double visualize_range_ = 2.0;

        esvo_core::PointCloud::Ptr pc_global = nullptr;

    public:
        void setAgeVisThreshold(double ageVisThreshold);

        void setInvDepthMaxRange(double invDepthMaxRange);

        void setInvDepthMinRange(double invDepthMinRange);

        void setStdVarVisThreshold(double stdVarVisThreshold);

        ESVOFuser(esvo_core::container::CameraSystem::Ptr &camSysPtr,
                  esvo_core::core::DepthProblemConfig::LS_Norm norm);

        esvo_core::container::DepthFrame::Ptr fuse(const slam::SparseDepthFramePtr& sparsePoints);

        void reset();

        void setPointCount(size_t pointCount);

        const esvo_core::PointCloud::Ptr& updateGlobalPointCloud(
                const esvo_core::container::DepthFrame::Ptr& depthFrame);



    };

}

