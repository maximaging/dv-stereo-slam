//
// Created by rokas on 2020-12-23.
//

#pragma once

#include "SafeQueue.h"

template<class T>
class MultiQueue {
private:
    std::vector<std::shared_ptr<SafeQueue<T>>> queues;
    typename std::vector<std::shared_ptr<SafeQueue<T>>>::iterator queueIter;

public:
    MultiQueue(int threads, size_t queueLength) {
        for (int i = 0; i < threads; i++) {
            auto q = std::make_shared<SafeQueue<T>>(queueLength);
            queues.push_back(q);
        }
        queueIter = queues.begin();
    }

    void setThreadOnCore(uint32_t threadId, int coreId) {
        if (threadId < queues.size()) {
            queues[threadId]->lockOnCpu(coreId);
        }
    }

    void enqueue(T value) {
        (*queueIter)->enqueue(value);

        // Round-robin execution
        queueIter++;
        if (queueIter == queues.end()) {
            queueIter = queues.begin();
        }
    }

    void enqueueThread(uint32_t threadId, T value) {
        if (threadId < queues.size()) {
            queues[threadId]->enqueue(value);
        }
    }

    void autoDequeue(std::function<void(const T&)> callback) {
        for (auto& q : queues) {
            q->autoDequeueAsync(callback);
        }
    }

    void stopAutoDequeue() {
        for (auto& q : queues) {
            q->stopAutoDequeue();
        }
    }

    [[nodiscard]] bool isRunning() const {
        // If at least one queue is active
        for (const auto& q : queues) {
            if (q->isRunning()) {
                return true;
            }
        }
        return false;
    }

    exception_ptr getLastException() {
        for (auto& q : queues) {
            if (auto lastException = q->getLastException()) {
                stopAutoDequeue();
                return lastException;
            }
        }
        return nullptr;
    }

    void reset() {
        for (auto &q : queues) {
            q->clear();
        }
    }
};

