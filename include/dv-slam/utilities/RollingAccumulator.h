//
// Created by rokas on 2021-01-21.
//

#pragma once

#include <dv-processing/core/frame.hpp>
#include "StampedMat.h"
#include "SafeEventStore.h"

namespace slam {

    class RollingAccumulator : public slam::SafeEventStore {
    private:
        size_t N, S, N_2;
        dv::PixelAccumulator accumulator;

        int64_t seekTime = 0LL;

        dv::EventStore::iterator lastIter;

    public:
        RollingAccumulator(int64_t timelimit, size_t n, size_t s, const cv::Size& dims);

        [[nodiscard]] std::vector<StampedMat> accumulate();

        [[nodiscard]] StampedMat accumulateWithin(int64_t start, int64_t stop);
    };

}
