//
// Created by rokas on 2020-10-08.
//

#pragma once

#include <boost/circular_buffer.hpp>
#include <esvo_core/tools/utils.h>
#include <opencv2/core/affine.hpp>

using namespace esvo_core::tools;

class Transformer {
private:
    typedef boost::circular_buffer<std::pair<int64_t, Transformation>> TransformBuffer;
    TransformBuffer tfBuffer = TransformBuffer(10000);

    TransformBuffer::const_iterator bufferLowBound(int64_t t) const {
        return std::lower_bound(tfBuffer.begin(), tfBuffer.end(), t,
                                [](const std::pair<int64_t, Transformation>& st, int64_t t){
                                    return st.first < t;
                                });
    }

    TransformBuffer::const_iterator bufferUpperBound(int64_t t) const {
        return std::upper_bound(tfBuffer.begin(), tfBuffer.end(), t,
                                [] (int64_t t, const std::pair<int64_t, Transformation>& st) {
                                    return t < st.first;
                                });
    }

    bool interpolateAt(int64_t timestamp, Transformation& outputTf) {
        auto lowBound = bufferLowBound(timestamp);
        if (lowBound != tfBuffer.end() && lowBound != tfBuffer.begin()) {
            auto previous = lowBound--;
            auto distance = static_cast<double>(lowBound->first - previous->first);
            auto lambda = 1.0 - (static_cast<double>(timestamp - previous->first) / distance);
            outputTf = kindr::minimal::interpolateComponentwise(lowBound->second, previous->second, lambda);
            return true;
        }
        return false;
    }

public:
    void pushTransform(int64_t timestamp, const Transformation& tf) {
        tfBuffer.push_back(std::make_pair(timestamp, tf));
    }

    bool getTransformAt(int64_t timestamp, Transformation& outputTf) {
        auto exactMatch = std::find_if(tfBuffer.begin(), tfBuffer.end(),
                                       [timestamp] (const std::pair<int64_t, Transformation>& st) {
                                           return st.first == timestamp;
                                       });
        // Find an exact match, otherwise interpolate
        if (exactMatch != tfBuffer.end()) {
            outputTf = exactMatch->second;
            return true;
        } else {
            return interpolateAt(timestamp, outputTf);
        }
    }

    void clear() {
        tfBuffer.clear();
    }

    [[nodiscard]] std::vector<cv::Affine3<double>> getAllPoses() const {
        std::vector<cv::Affine3<double>> poses;
        for (const auto &tf : tfBuffer) {
            cv::Mat tr;
            cv::eigen2cv(tf.second.getTransformationMatrix(), tr);
            poses.emplace_back(tr);
        }
        return poses;
    }


    [[nodiscard]] bool empty() const {
        return tfBuffer.empty();
    }

    [[nodiscard]] const std::pair<int64_t, Transformation>& latestTransform() const {
        return tfBuffer.back();
    }

};

